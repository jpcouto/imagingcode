#!/usr/bin/env python
# Install script imaging code tools
# Joao Couto - May 2017

import os
from os.path import join as pjoin
from setuptools import setup
from setuptools.command.install import install


longdescription = ''' Analysis and tools for calcium imaging experiments '''
data_path = pjoin(os.path.expanduser('~'), 'imagingCode')

setup(
  name = 'imagingCode',
  version = '0.0',
  author = 'Joao Couto',
  author_email = 'jpcouto@gmail.com',
  description = (longdescription),
  long_description = longdescription,
  license = 'GPL',
  packages = ['imaging',
              'imaging/registration',
              'imaging/analysis',
              'imaging/scripts'],
  entry_points = {
        'console_scripts': [
            '2p-rec-reg = imaging.scripts.preprocessing:main',
#            '2p-sbx-reg = imaging.scripts.preprocessing_scanbox:main',
            '2p-sbx-reg = imaging.scripts.reg_scanbox_dask:main',
            '2p-sbx-reg-cluster = imaging.scripts.reg_scanbox_dask:run_job',
            '2p-sbx-reg2 = imaging.scripts.reg_scanbox_dask_caiman:main',
            '2p-sbx-reg-cluster2 = imaging.scripts.reg_scanbox_dask_caiman:run_job',
            '2p-caiman-cnmf = imaging.scripts.caiman_cnmf:main',
            '2p-sbatch-caiman = imaging.scripts.caiman_cnmf:run_job',
            '2p-run-suite2p = imaging.scripts.suite2p_wrapper:cli_suite2p',
            '2p-sbatch-suite2p = imaging.scripts.suite2p_wrapper:cli_suite2p_job',
            'sbx-viewer = imaging.scripts.sbx_viewer:main',
            'plot-behavior-from-presentation = imaging.presentation:main',
        ]
        },
#    data_files=[(data_path, [pjoin('configurations',
#                                   'spyking-circus-invivo.params')]),
#    ],

    )
