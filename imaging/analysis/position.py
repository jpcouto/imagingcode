import numpy as np
from tqdm import tqdm

def binToLaps(laps,time,position,X,lapspace = None,
              velocity = None,
              velocityThreshold = 3., beltLength=150.,
              method=lambda x : np.mean(x,axis=1)):
    '''
    Bins vector X to lap position.
        - laps is [numberOfLaps,2] the start and stop time of each lap
    
    '''
    if lapspace is None:
        lapspace = np.arange(0,beltLength,1.)
    lapX = np.zeros((X.shape[0],lapspace.shape[0]-1,laps.shape[0]))
    lapX[:] = np.nan
    for k,l in tqdm(enumerate(range(laps.shape[0])),desc='Binning to lap '):
        s,e = laps[l,:]
        idx = np.where((time>=s) & (time<e))[0].astype(int)
        x = X[:,idx]
        pos = position[idx]
        inds = np.digitize(pos, lapspace)
        for i in np.unique(inds)[:-1]:
            lapX[:,i-1,k] = method(x[:,(inds==i)])
    return lapX
