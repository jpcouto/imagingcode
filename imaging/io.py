try:
    from tifffile import TiffFile,imread
except:
    pass
import os
import numpy as np
from scipy.io import loadmat

from tqdm import tqdm

__all__ = ['TiffStack','sbxGetShape','sbxMemmap']

class TiffStack(object):
    def __init__(self,filenames):
        assert type(filenames) is list, 'Pass a list of filenames.'
        self.filenames = filenames
        for f in filenames:
            assert os.path.exists(f), f + ' not found.'
        # Get an estimate by opening only the first and last files
        framesPerFile = []
        self.files = []
        for i,fn in enumerate(self.filenames):
            if i == 0 or i == len(self.filenames)-1:
                self.files.append(TiffFile(fn))
            else:
                self.files.append(None)                
            f = self.files[-1]
            if i == 0:
                dims = f.series[0].shape
                self.shape = dims
            elif i == len(self.filenames)-1:
                dims = f.series[0].shape
            framesPerFile.append(np.int64(dims[0]))
        self.framesPerFile = np.array(framesPerFile, dtype=np.int64)
        self.framesOffset = np.hstack([0,np.cumsum(self.framesPerFile[:-1])])
        self.nFrames = np.sum(framesPerFile)
        self.curfile = 0
        self.curstack = self.files[self.curfile].asarray(memmap=False)
        N,self.h,self.w = self.curstack.shape[:3]
        self.dtype = self.curstack.dtype
        self.shape = (self.nFrames,self.shape[1],self.shape[2])
        print('Loaded TiffStack [{0} frames]'.format(self.shape[0]))
    def getFrameIndex(self,frame):
        '''Computes the frame index from multipage tiff files.'''
        fileidx = np.where(self.framesOffset <= frame)[0][-1]
        return fileidx,frame - self.framesOffset[fileidx]
    def __getitem__(self,*args):
        index  = args[0]
        start, stop, step = index.indices(self.nFrames)
        index = range(start, stop, step)
        img = np.empty((len(index),self.h,self.w),dtype = self.dtype)
        for i,ind in enumerate(index):
            img[i,:,:] = self.getFrame(ind)
        return img    
    def getFrame(self,frame):
        ''' Returns a single frame from the stack '''
        fileidx,frameidx = self.getFrameIndex(frame)
        if not fileidx == self.curfile:
            if self.files[fileidx] is None:
                self.files[fileidx] = TiffFile(fn)
            self.curstack = self.files[fileidx].asarray(memmap=False)
            self.curfile = fileidx
        return self.curstack[frameidx,:,:]
    def __len__(self):
        return self.nFrames

# SCANBOX
    
def sbxMemmap(filename):
    if filename[-3:] == 'sbx':
        sbxshape,nplanes = sbxGetShape(filename)
    return np.memmap(filename,dtype='uint16',shape=sbxshape,order='F')

def sbxGetShape(sbxfile):
    matfile = os.path.splitext(sbxfile)[0] + '.mat'
    if not os.path.exists(matfile):
        raise('Metadata not found: {0}'.format(matfile))
    info = loadmat(matfile)
    info = info['info']
    fsize = os.path.getsize(sbxfile)
    nrows,ncols = info['sz'][0][0][0]
    chan = info['channels'][0][0][0]
    recordsPerBuffer = info['recordsPerBuffer'][0][0][0]
    if chan == 1:
        chan = 2; factor = 1
    elif chan == 2:
        chan = 1
        factor = 2
    elif chan == 3:
        chan = 1
        factor = 2
    if info['scanmode'][0][0][0]==0:
        recordsPerBuffer *= 2
    max_idx = os.path.getsize(sbxfile)/recordsPerBuffer/ncols*factor/4
    nSamples = ncols * recordsPerBuffer * 2 * chan
    if len(info['otwave'][0][0]):
        nplanes = len(info['otwave'][0][0][0])
    else:
        nplanes = 1
    return (int(chan),int(ncols),int(recordsPerBuffer),int(max_idx)),nplanes
