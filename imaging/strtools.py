import re

def strNumberSort(iterable):
    """Return human sorted list of strings.

    E.g. for sorting file names.

    >>> strNumberSort(['f1', 'f2', 'f10'])
    ['f1', 'f2', 'f10']
    This is from tifffile by Christoph Gohlke
    """
    def sortkey(x):
        return [(int(c) if c.isdigit() else c) for c in re.split(numbers, x)]
    numbers = re.compile(r'(\d+)')
    return sorted(iterable, key=sortkey)
