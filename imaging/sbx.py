from multiprocessing import Process,Array,Value,Event
from ctypes import c_uint16
import struct
import numpy as np
import mmap
import cv2
import numpy as np
from .registration import dftreg_TranslationShift,shiftImage
from tifffile import imsave,imread
from os.path import join as pjoin
from glob import glob
import os

class ScanboxMmap(Process):
    def __init__(self,
                 scanboxmmapfile = 'C:/Users/bstation6/Documents/Scanbox/mmap/scanbox.mmap',
                 nplanes = 4,
                 bufsize = None,
                 nbuffers = 5):
        super(ScanboxMmap,self).__init__()
        self.scanboxfile = scanboxmmapfile
        self.active = Event()
        self.connected = Event()
        self.register = Event()
        self.bufsize = bufsize
        if self.bufsize is None:
            mmapfile = open(self.scanboxfile,'r+b')
            sbxmap = mmap.mmap(mmapfile.fileno(),0)
            h,w,n = struct.unpack('3h', sbxmap[2:8])
            self.bufsize = [h,w]
            mmapfile.close()
            sbxmap.close()
        self.nbuffers = nbuffers
        
        self.frame_buffers = [Array(c_uint16,np.zeros(self.bufsize,
                                                      np.uint16).flatten()) for i in range(self.nbuffers)]
        
        self.frame_num = Value('i',0)
        
        self.h = Value('i',0)
        self.w = Value('i',0)
        self.iPlane  = Value('i',0)
        self.nPlanes  = Value('i',1)
        self.frame = [np.frombuffer(f.get_obj(),
                                   dtype = np.uint16).reshape(self.bufsize) for f in self.frame_buffers]
        self.daemon = True
        self.active.set()
        self.start()

    def run(self):
        print('Running')
        while self.active.is_set():
            mmapfile = open(self.scanboxfile,'r+b')
            print('Opened scanbox file')

            sbxmap = mmap.mmap(mmapfile.fileno(),0)
            print('Did memory mapping')
            switch = struct.unpack('h', sbxmap[:2])
            self.connected.set()
            framebuf = [np.frombuffer(f.get_obj(),
                                     dtype = np.uint16).reshape(self.bufsize) for f in self.frame_buffers]
            while self.connected.is_set():
                switch = struct.unpack('h', sbxmap[:2])[0]
                if switch > -1:
                    h,w,n = struct.unpack('3h', sbxmap[2:8])
                    self.h = h
                    self.w = w
                    tmp = np.ndarray((w,h),
                                     dtype = np.uint16,
                                     buffer = sbxmap[16:16+2*h*w]).T.copy()
                    self.frame_num.value = switch
                    framebuf[np.mod(switch,self.nPlanes.value)][:h,:w] = 65535 - tmp[:,:]
                    sbxmap[:2] = np.int16(-1).tobytes()
                elif switch == -2:
                    print('Scanbox stopped.')
            sbxmap.close()
            mmapfile.close()


class ScanboxRollingAverage(object):
    showref = False
    register = False
    equalize  = True
    superimpose = False
    flipimage = True
    iPlane = 0
    def __init__(self,
                 scanboxmmapfile = 'C:/Users/bstation6/Documents/Scanbox/mmap/scanbox.mmap',
                 nplanes = 4,
                 bufsize = None,
                 nbuffers = 5):
        super(ScanboxRollingAverage,self).__init__()

        # start the memory map
        # This object needs to be destroyed if the number of lines changes
        
        self.nPlanes = nplanes
        self.sbxmmap = ScanboxMmap()
        self.sbxmmap.nPlanes.value = self.nPlanes
        self.nFrames = 8
        self.alpha_ema = 1./self.nFrames
        self.references = [None for r in range(self.sbxmmap.nPlanes.value)]
        self.clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        self.scale = 1
        # can have a different reference per plane.
        self.externalImages = [None for r in range(self.nPlanes)]

    def play(self):
        cv2.startWindowThread()
        i = 0
        while(True):
            frame = self.sbxmmap.frame[self.iPlane]
            i = self.sbxmmap.frame_num.value
            
            if self.references[self.iPlane] is None:
                self.references[self.iPlane] = frame.copy()
            if self.register:
                shift,_,_ = dftreg_TranslationShift(
                    self.references[self.iPlane][200:-200:2,200:-200:1],
                    frame[200:-200:2,200:-200:1],
                    upsample_factor=2)
                frame = shiftImage(frame,shift)
            # the rolling average
            self.references[self.iPlane]  = ((1.-self.alpha_ema)*self.references[self.iPlane].astype(np.float32) + 
                                   (self.alpha_ema)*frame.astype(np.float32)).astype(np.uint16)
            # Convert to image
            img = cv2.convertScaleAbs(self.references[self.iPlane],
                                      alpha=(255.0/65535.0))
    
            if self.equalize:
                img = self.clahe.apply(img.astype(np.uint8))
            # Convert to color
            img = cv2.cvtColor(img.astype(np.uint8), cv2.COLOR_GRAY2BGR)
        
            if self.superimpose:
                if not self.externalImages[self.iPlane] is None:
                    img[:,:,0] = 0
                    img[:,:,2] = 0 # green image
                    otheri = cv2.convertScaleAbs(self.externalImages[self.iPlane],
                                                 alpha=(255.0/65535.0))
                    # estimate the shift to the other image
                    shifto,_,_ = dftreg_TranslationShift(
                        self.references[self.iPlane][200:-200:2,200:-200:1],
                        self.externalImages[self.iPlane][200:-200:2,200:-200:1],
                        upsample_factor=2)
                    if self.equalize:
                        otheri = self.clahe.apply(otheri.astype(np.uint8))
                    # red image
                    img[:,:,2] = otheri[:,:]
            #rescale
            img = cv2.resize(img,(0,0),fx=self.scale,
                             fy = self.scale)
            if self.showref and self.superimpose and not self.externalImages[self.iPlane] is None:
                img[:,:,1] = 0
                
            if self.flipimage:
                img = img.transpose([1,0,2]).astype(np.uint8).copy()
                
            if self.superimpose and not self.externalImages[self.iPlane] is None:
                cv2.putText(img,'{0},{1}'.format(shifto[0],shifto[1]), (10,300),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            1, [200,200,200],2)
            
            if self.register:
                color = [255,20,20]
                cv2.putText(img,'{0} - {1},{2}'.format(i,shift[0],shift[1]), (10,100),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            1, color,2)
            else:
                color = [255,0,255]
                cv2.putText(img,'{0}'.format(i), (10,100),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            1, color,2)
    
    
            cv2.imshow('scanbox rolling average',img)
            k = cv2.waitKey(1)
            if k == ord('q'):  # if no key was pressed, -1 is returned
                break
            elif k == ord('r'):
                self.register = not self.register
            elif k == ord('e'):
                self.equalize = not self.equalize
            elif k == ord('s'):
                self.superimpose = not self.superimpose
            elif k == ord('a'):
                self.showref = not self.showref
            elif k == ord('d'):
                self.scale *= 0.8
                print(self.scale)
            elif k == ord('u'):
                self.scale *= 1.2
            elif k == ord('w'):
                self.references[self.iPlane] = None
            elif k == ord('o'):
                self.nFrames -= 1
                self.nFrames = np.clip(self.nFrames,1,200)
                self.alpha_ema = 1./self.nFrames
            elif k == ord('p'):
                self.nFrames += 1
                self.nFrames = np.clip(self.nFrames,1,200)
                self.alpha_ema = 1./self.nFrames
                print(self.nFrames,self.alpha_ema)
            elif k == ord('+'):
                self.iPlane += 1
                self.iPlane = np.clip(self.iPlane,0,self.nPlanes - 1)
            elif k == ord('-'):
                self.iPlane -= 1
                self.iPlane = np.clip(self.iPlane,0,self.nPlanes - 1)
            elif not k == -1:
                print(k)
                #    break
        # When everything done, release the capture
        cv2.destroyAllWindows()
    def saveReferences(self,expname,filename,parentfolder = 'i://data/references/'):
        foldername = pjoin(parentfolder,expname)
        if not os.path.isdir(foldername):
            print('Creating {0}'.format(foldername))
            os.makedirs(foldername)
        files = glob(pjoin(foldername,'ref*.tif'))
        iref = len(files)
        otherimg = np.stack(self.references,axis = 0)
        fname = 'ref{0:03d}_plane{1:03d}_{2}.tif'.format(iref,self.nPlanes,filename)
        filename = pjoin(foldername,fname)
        imsave(filename,otherimg,photometric = 'minisblack', imagej = False)
        print('Saved {0} planes - {1}'.format(len(otherimg),filename))
    def loadReferences(self,fname):
        refs = imread(fname)
        if len(refs.shape) == 3:
            for iPlane,ref in enumerate(refs):
                self.externalImages[iPlane] = ref
        else:
            self.externalImages[0] = refs
        self.superimpose = True
        
    def close(self):
        self.sbxmmap.connected.clear()
        self.sbxmmap.active.clear()
        
    

def sbx_get_info(sbxfile):
    ''' 
    Read info from a scanbox mat file [pass the sbx extension].
    info = sbx_get_info(sbxfile)
    '''
    matfile = os.path.splitext(sbxfile)[0] + '.mat'
    if not os.path.exists(matfile):
        raise('Metadata not found: {0}'.format(matfile))
    from scipy.io import loadmat
    info = loadmat(matfile,squeeze_me=True,struct_as_record=False)
    return info['info']

def sbx_get_shape(sbxfile):
    ''' 
    Get shape from scanbox file.
    Reads it from the file size and the info mat file.
    (chan,ncols,nrows,max_idx),nplanes = sbx_get_shape(sbxfile)
    '''
    info = sbx_get_info(sbxfile)
    fsize = os.path.getsize(sbxfile)
    nrows,ncols = info.sz
    chan = info.channels
    if chan == 1:
        chan = 2; 
    elif chan == 2:
        chan = 1
    elif chan == 3:
        chan = 1
    max_idx = os.path.getsize(sbxfile)/nrows/ncols/chan/2
    if max_idx != info.config.frames:
        raise(Warning('sbx filesize doesnt match mat [{0},{1}]'.format(
                    max_idx,
                    info.config.frames)))
    if len(info.otwave) and info.volscan:
        nplanes = len(info.otwave)
    else:
        nplanes = 1
    return (int(chan),int(ncols),int(nrows),int(max_idx)),nplanes

#def sbx_get_shape(sbxfile):
#    ''' 
#    Get shape from scanbox file.
#    Reads it from the file size and the info mat file.
#    (chan,ncols,recordsperbuffer,max_idx),nplanes = sbx_get_shape(sbxfile)
#    '''
#    info = sbx_get_info(sbxfile)
#    fsize = os.path.getsize(sbxfile)
#    nrows,ncols = info['sz'][0][0][0]
#    chan = info['channels'][0][0][0]
#    recordsPerBuffer = info['recordsPerBuffer'][0][0][0]
#    if chan == 1:
#        chan = 2; factor = 1
#    elif chan == 2:
#        chan = 1
#        factor = 2
#    elif chan == 3:
#        chan = 1
#        factor = 2
#    if info['scanmode'][0][0][0]==0:
#        recordsPerBuffer *= 2
#    max_idx = os.path.getsize(sbxfile)/recordsPerBuffer/ncols*factor/4
#    nSamples = ncols * recordsPerBuffer * 2 * chan
#    if len(info['otwave'][0][0]):
#        nplanes = len(info['otwave'][0][0][0])
#    else:
#        nplanes = 1
#    return (int(chan),int(ncols),int(recordsPerBuffer),int(max_idx)),nplanes

def sbx_memmap(filename,reshape_planes=True):
    '''
    Memory maps a scanbox file.
    sbxmmap = sbx_memmap(filename,reshape_planes=True)
    Reshapes file to the appropriate shape (N,nplanes,nchan,H,W).reshape_planes does nothing for now.
    Actual 2P data is 65535 - sbxmmap.
    '''
    if filename[-3:] == 'sbx':
        sbxshape,nplanes = sbx_get_shape(filename)
        return np.memmap(filename,dtype='uint16',shape=sbxshape,order='F').transpose([3,0,2,1]).reshape(
            int(sbxshape[3]/nplanes),
            nplanes,
            sbxshape[0],
            sbxshape[2],
            sbxshape[1])
    else:
        raise ValueError('Not sbx:  '+ filename)
