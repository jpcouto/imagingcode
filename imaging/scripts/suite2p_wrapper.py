# script to submit suite2p jobs on the cluster...

import sys
import os
from os.path import join as pjoin
from glob import glob
import numpy as np
import time

# set your options for running
# overwrites the run_s2p.default_ops
ops = {'fast_disk': '/tmp/suite2p', # used to store temporary binary file, defaults to save_path0
       'save_path0': [], # stores results, defaults to first item in data_path
       'delete_bin': True, # whether to delete binary file after processing
       # main settings
       'nplanes' : 1, # each tiff has these many planes in sequence
       'nchannels' : 1, # each tiff has these many channels per plane
       'functional_chan' : 1, # this channel is used to extract functional ROIs (1-based)
       'diameter':12, # this is the main parameter for cell detection, 2-dimensional if Y and X are different (e.g. [6 12])
       'tau':  1., # this is the main parameter for deconvolution
       'fs': 10.,  # sampling rate (total across planes)
       # output settings
       'save_mat': False, # whether to save output as matlab files
       'combined': False, # combine multiple planes into a single result /single canvas for GUI
       # parallel settings
       'num_workers': 0, # 0 to select num_cores, -1 to disable parallelism, N to enforce value
       'num_workers_roi': 0, # 0 to select number of planes, -1 to disable parallelism, N to enforce value
       # registration settings
       'do_registration': False, # whether to register data
       'nimg_init': 512, # subsampled frames for finding reference image
       'batch_size': 256, # number of frames per batch
       'maxregshift': 0.1, # max allowed registration shift, as a fraction of frame max(width and height)
       'align_by_chan' : 1, # when multi-channel, you can align by non-functional channel (1-based)
       'reg_tif': False, # whether to save registered tiffs
       'subpixel' : 10, # precision of subpixel registration (1/subpixel steps)
       # cell detection settings
       'connected': True, # whether or not to keep ROIs fully connected (set to 0 for dendrites)
       'navg_frames_svd': 5000, # max number of binned frames for the SVD
       'nsvd_for_roi': 1000, # max number of SVD components to keep for ROI detection
       'max_iterations': 20, # maximum number of iterations to do cell detection
       'ratio_neuropil': 6., # ratio between neuropil basis size and cell radius
       'ratio_neuropil_to_cell': 3, # minimum ratio between neuropil radius and cell radius
       'tile_factor': 1., # use finer (>1) or coarser (<1) tiles for neuropil estimation during cell detection
       'threshold_scaling': 1., # adjust the automatically determined threshold by this scalar multiplier
       'max_overlap': 0.75, # cells with more overlap than this get removed during triage, before refinement
       'inner_neuropil_radius': 2, # number of pixels to keep between ROI and neuropil donut
       'outer_neuropil_radius': np.inf, # maximum neuropil radius
       'min_neuropil_pixels': 350, # minimum number of pixels in the neuropil
       # deconvolution settings
       'baseline': 'maximin', # baselining mode
       'win_baseline': 60., # window for maximin
       'sig_baseline': 10., # smoothing constant for gaussian filter
       'prctile_baseline': 8.,# optional (whether to use a percentile baseline)
       'neucoeff': .7,  # neuropil coefficient
}

db = {'h5py': [], # a single h5 file path
      'h5py_key': 'data',
      'look_one_level_down': True, # whether to look in ALL subfolders when searching for tiffs
      'data_path': [ ], # a list of folders with tiffs 
      # (or folder of folders with tiffs if look_one_level_down is True, or subfolders is not empty)
      'subfolders': [] # choose subfolders of 'data_path' to look in (optional)
}

def run_suite2p(datafolder,
                savepath = None,
                nplanes = 1,
                nchannels = 1,
                fs = 30.,
                tau = 1.,
                reg = False,
                **kwargs):
    db['data_path'] = [datafolder]
    ops['data_path'] = [datafolder]
    ops['nplanes'] = nplanes
    ops['fs'] = fs
    ops['do_registration'] = reg
    from suite2p.run_s2p import run_s2p    
    return run_s2p(ops=ops,db=db)

def cli_suite2p():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Runs suite2p for a single plane.')
    parser.add_argument('folder',metavar='foldername',
                        type=str,
                        help='single plane, single channel folder with reg data')
    parser.add_argument('--fs',
                        action='store',
                        default=30.,
                        type=float,
                        help='sampling frequency')
    # parser.add_argument('--nplanes',
    #                     action='store',
    #                     default=1,
    #                     type=int,
    #                     help='number of planes')
    parser.add_argument('--reg',
                        action='store_true',
                        default=False,
                        help='do registration')
    
    opts = parser.parse_args()
    
    run_suite2p(opts.folder,fs = opts.fs,reg = opts.reg)

def cli_suite2p_job():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Cluster submission script for suite2p')
    parser.add_argument('folder',metavar='foldername',
                        type=str,help='folder with reg data from multiple runs')
    parser.add_argument('--fs',
                        action='store',
                        default=30.,
                        type=float,
                        help='sampling frequency')
    parser.add_argument('--reg',
                        action='store_true',
                        default=False,
                        help='do registration')
    
    
    opts = parser.parse_args()
    tt = list(filter(os.path.isdir,glob(pjoin(opts.folder,'*'))))
    folders = np.sort(tt)
    planes = []
    if len(folders):
        for f in folders:
            pln = glob(pjoin(f,'plane*'))
            planes.append([p for p in np.sort(list(filter(os.path.isdir,pln)))])
    else:
        raise(OSError("Check folder: {0}".format(opts.folder)))
    import subprocess as sub
    nplanes = len(planes[0])
    fr = opts.fs/float(nplanes)
    for irun,planefolders in enumerate(planes):
        for iplane,planefolder in enumerate(planefolders):
            
            cmd = '2p-run-suite2p {0} --fs {1}'.format(planefolder,fr)
            if opts.reg:
                cmd += ' --reg'
            jobname = 'suite2p_{0}'.format(os.path.basename(opts.folder))
            ntasks = 1
            ncpuspertask = 40
            memory = None
            walltime = None
            partition = 'long'
            conda_environment = None
            module_environment = None
            mail = None
            workdir = os.path.abspath(pjoin(os.path.expanduser('~'),'dask-temp'))
            sbatch_append = ''
            sjobfile = '''#!/bin/bash -login
#SBATCH --job-name={0}
#SBATCH --output=log_%j.out
#SBATCH --error=log_%j.err
    
#SBATCH --ntasks={1}
#SBATCH --cpus-per-task={2}
'''.format(jobname,ntasks,ncpuspertask)
            if not walltime is None:
                sjobfile += '#SBATCH --time={0} \n'.format(walltime)
            if not memory is None:
                sjobfile += '#SBATCH --mem={0} \n'.format(memory)
            if not partition is None:
                sjobfile += '#SBATCH --partition={0} \n'.format(partition)
            if not mail is None:
                sjobfile += '#SBATCH --mail-user={0} \n#SBATCH --mail-type=END,FAIL \n'.format(mail)
            if not module_environment is None:
                sjobfile += '\n module purge\n'
                sjobfile += '\n module load {0} \n'.format(module_environment)
            if not conda_environment is None:
                sjobfile += 'source activate {0} \n'.format(conda_environment)
            sjobfile += '''echo JOB STARTED `date`
module purge
module load conda-local
source deactivate
source activate 2p-py3
cd {0}
{1}
echo JOB FINISHED `date`
'''
            batchfname = pjoin(workdir,
                               'suite2p_'+'-'.join(opts.folder.split(os.path.sep))+
                               '{0:02d}.sh'.format(nplanes))
            with open(batchfname,'w') as f:
                f.write(sjobfile.format(workdir,cmd))
            sub.call('sbatch ' + batchfname,shell=True)
            print(cmd)
            os.remove(batchfname)
        
