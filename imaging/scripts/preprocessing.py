# Parallel reconstruction and registration of 2P data

from mpi4py import MPI
import sys
import os
from os.path import join as pjoin
from glob import glob
import numpy as np
from scipy.stats import mode
import time
from shutil import rmtree
from tifffile import imread, imsave
from tqdm import tqdm
from imaging import (strNumberSort,reconstruct2PTifFile,
                     reconstruct2PStack,estimate2PLineShift,
                     register2PFrames)
import cv2
cv2.setNumThreads(1)

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def main():
    desc = 'Reconstruction and 2D registration of 2P data.'

#    if  MPI.Get_version()[0]<3:
#        print("Please use MPI 3.0+ to avoid buffer overflow issues.")
#        sys.exit(1)
    if size < 2:
        print('''
        {0}
        Need at least 2 MPI workers...
              Run it with MPI like this:
                mpiexec -n 12 imaging-recReg2P <FOLDERNAME>
        Or use the nerf-cluster submission tools.
        '''.format(desc))
        sys.exit(1)
    params = {}
    if rank == 0:
        print('Running with {0} workers.'.format(size))
        from argparse import ArgumentParser
        parser = ArgumentParser(description=desc)
        parser.add_argument('folder',metavar='foldername',
                            type=str,help='folder with raw 2P data (takes tiffs or sbx data)',
                            nargs=1)
        parser.add_argument('-o','--output-folder',metavar='destination',
                            default=None,
                            type=str,help='folder to place reg data',
                            nargs=1)
        parser.add_argument('--temp',metavar='temporary directory',
                            default=['/tmp'],
                            type=str,help='folder to place reg data',
                            nargs=1)
        parser.add_argument('--runs',metavar='run indexes',
                            action='store',
                            type=int,
                            nargs='+',
                            default=[],
                            help='runs to reconstruct/register')
        parser.add_argument('--no-reconstruction',
                            action='store_false',
                            default=True,
                            help='skips reconstruction')
        opts = parser.parse_args()
        exppath = opts.folder[0]
        if opts.output_folder is None:
            destfolder = os.path.abspath(os.path.curdir)
            print('Output not defined, using current folder: {0}'.format(destfolder))
        else:
            destfolder = opts.output_folder[0]
        selectedRuns = opts.runs
        tempdir = opts.temp[0]
        nRefFiles = 10

        if os.path.isdir(exppath):
            tmp = strNumberSort(os.listdir(exppath))
            # Only folders with tif files
            runs = []
            for f in tmp:
                if os.path.isdir(pjoin(exppath,f)):
                    if (len(glob(pjoin(exppath,f,'*.tif'))) > 10):
                        runs.append(f)
                    else:
                        print('Discarding run:' + f)
            # Search for tif files
            files = []
            for run in runs:
                files.append(np.array(strNumberSort(glob(pjoin(exppath,run,'*.tif')))))
#            files = np.concatenate(files)
            # do only channel 1 for now.
            basenames = []
            for ii,fs in enumerate(files):
                basenames.append(np.array([os.path.basename(f)  for f in fs]))
                sel = np.array([i for i, v in enumerate(basenames[ii]) if '_ch2_' in v])
                basenames[ii] = basenames[ii][sel]
                files[ii] = fs[sel]

            if not len(selectedRuns):
                selectedRuns = range(len(runs))
            runs = [runs[i] for i in selectedRuns]
            files = [files[i] for i in selectedRuns]
            basenames = [basenames[i] for i in selectedRuns]
            filerun = np.concatenate([[run]*len(fname) for run,fname in zip(runs,files)])
            files = np.concatenate(files)
            basenames = np.concatenate(basenames)
            params = dict(filepaths = files,
                          filenames = basenames,
                          runname = filerun,
                          runs = runs,
                          tempdir = tempdir,
                          destfolder = destfolder,
                          expname = os.path.split(exppath)[-1],
                          plane = np.ones_like(files,dtype = 'int'),
                          channel = np.ones_like(files,dtype = 'int')*2,
                          filesForReference = files[len(files)/2-nRefFiles:len(files)/2 +nRefFiles])
            print('''
            Experiment: {0}
            Temp directory: {1}
            Output path: {2}
            Runs: {3}
            Files: {4}
            '''.format(params['expname'],
                       params['tempdir'],
                       params['destfolder'],
                       len(params['runs']),
                       len(params['filenames'])))
    params = comm.bcast(params, root=0)
    comm.barrier()
    nFiles = len(params['filenames'])
    filesPerWorker = int(np.ceil((nFiles+1)/float(size)))
    offset = filesPerWorker*rank
    if rank == 0:
        print('Starting reconstruction of {0} files [Files per worker {1}].'.format(
            nFiles,filesPerWorker))
        tstart = time.time()
        tbegin = MPI.Wtime()
    tmpdir = pjoin(params['tempdir'],'rec_reg_{1}_worker{0:0d}'.format(
        int(time.time()),rank))
    os.makedirs(tmpdir)
    # Estimate shift delay for reconstruction
    data = []
    run = []
    shiftEstimate = np.zeros((len(params['runs']),))
    shiftEstimate[:] = np.nan
    for i in range(2): # Each worker does 2 files to estimate shift
        iFile = i+offset
        if iFile<nFiles:
            fname = params['filenames'][iFile]
            fpath = params['filepaths'][iFile]
            try:
                dat = imread(fpath)
            except ValueError:
                print('Error: Empty tiff?!? {0}'.format(fpath))
                raise
            data.append(reconstruct2PStack(dat))
            run.append(params['runs'].index(params['runname'][iFile]))
    if len(data):
        if len(np.unique(run)) == 1:
            data = np.concatenate(data,axis=0)        
            shiftEstimate[run[0]] = estimate2PLineShift(data)
            print('[{0}] - got shift {1}'.format(rank,
                                                 shiftEstimate))
    shift = comm.gather(shiftEstimate,root = 0)
    if rank == 0:
        shift = mode(np.vstack(shift)).mode[0]
        # shift = mode(shift[shift != np.nan]).mode[0]
        shiftEstimate = shift.astype(int)
        print('[{0}] - Computed shifts {1} per run starting reconstruction.'.format(rank,shift))
    shiftEstimate = comm.bcast(shiftEstimate, root=0)
    # Reconstruct all data!
    recdata = []
    rec_ed_files = 0
    if rank == 0:
        pbar = tqdm(total=filesPerWorker)
    for i in range(filesPerWorker):
        iFile = i+offset
        if iFile<nFiles:
            fname = params['filenames'][iFile]
            fpath = params['filepaths'][iFile]
            reconstruct2PTifFile([fpath,pjoin(tmpdir,fname)],
                                 shift=shiftEstimate[params['runs'].index(params['runname'][iFile])])
            rec_ed_files += 1
            if rank == 0:
                pbar.update(1)
    if rank == 0:
        pbar.close()
    #print('[{0}] - Reconstructed {1} files'.format(rank,
    #                                               rec_ed_files))
    comm.barrier()
    if rank == 0:
        tstop = time.time()
        print('[{0}] - done with reconstruction in {1} secs computing reference'.format(rank,tstop-tstart))
        tstart = time.time()
    # Estimate average 
    means = []
    for i in range(filesPerWorker):
        iFile = i+offset
        if iFile<nFiles:
            fname = params['filenames'][iFile]
            fpath = params['filepaths'][iFile]
            if fpath in params['filesForReference']:
                print('[{0}] - Averaging {1}'.format(rank,
                                                     fname))
                dat = imread(pjoin(tmpdir,fname))
                means.append(dat.mean(axis=0))
    if len(means):
        means = np.dstack(means).mean(axis=2)
    else:
        means = None
    # Compute the reference plane
    meansReduce = comm.gather(means,root = 0)
    ref = []
    if rank == 0:
        meansReduce = np.dstack([m for m in meansReduce if not m is None])
        ref = meansReduce.mean(axis = 2)
        print('[{0}] - Got average plane'.format(rank))
    ref = comm.bcast(ref, root=0)
    # Register to average
    means = []
    for i in range(filesPerWorker):
        iFile = i+offset
        if iFile<nFiles:
            fname = params['filenames'][iFile]
            fpath = params['filepaths'][iFile]
            if fpath in params['filesForReference']:
                #print('[{0}] - Registering {1}'.format(rank,
                #                                  fname))
                dat = imread(pjoin(tmpdir,fname))
                img,drift = register2PFrames(ref,
                                             dat,
                                             discardBorder=100,
                                             upsampleFactor=2)
                means.append(img.mean(axis=0))
    if len(means):
        means = np.dstack(means).mean(axis=2)
    else:
        means = None

    # Compute the reference plane
    meansReduce = comm.gather(means,root = 0)
    ref = []
    if rank == 0:
        meansReduce = np.dstack([m for m in meansReduce if not m is None])
        ref = meansReduce.mean(axis = 2)
        print('[{0}] - Got reference plane'.format(rank))
    ref = comm.bcast(ref, root=0)
    # Register all stack
    if rank == 0:
        pbar = tqdm(total=filesPerWorker)
    means = []
    for i in range(filesPerWorker):
        iFile = i+offset
        if iFile<nFiles:
            fname = params['filenames'][iFile]
            fpath = params['filepaths'][iFile]
            #print('[{0}] - Registering {1} [run: {2}]'.format(rank,
            #                                                  fname,
            #                                                  params['runname'][iFile]))
            dat = imread(pjoin(tmpdir,fname))
            img,drift = register2PFrames(ref,
                                         dat,
                                         discardBorder=100,
                                         upsampleFactor=2)
            if params['plane'][iFile] > 1:
                targetfile = pjoin(params['destfolder'],
                                   params['expname'],
                                   params['runname'][iFile],
                                   'plane{0:02d}_ch{1:02d}'.format(
                                       params['plane'][iFile],
                                       params['channel'][iFile]),
                                   'reg_plane{0:02d}_ch{1:02d}_{2}'.format(
                                       params['plane'][iFile],
                                       params['channel'][iFile],
                                       fname))
            else:
                targetfile = pjoin(params['destfolder'],
                                   params['expname'],
                                   params['runname'][iFile],
                                   'reg_plane{0:02d}_ch{1:02d}_{2}'.format(
                                       params['plane'][iFile],
                                       params['channel'][iFile],
                                       fname))
            try:
                if not os.path.isdir(os.path.dirname(targetfile)):
                    os.makedirs(os.path.dirname(targetfile))
            except:
                pass
            imsave(targetfile,img)
            means.append(img.mean(axis=0))
            if rank == 0:
                pbar.update(1)

    if rank == 0:
        pbar.close()
    if len(means):
        means = np.dstack(means).mean(axis=2)
    else:
        means = None
    meansReduce = comm.gather(means,root = 0)
    ref = []
    comm.barrier()
    if rank == 0:
        meansReduce = np.dstack([m for m in meansReduce if not m is None])
        avgStack = meansReduce.mean(axis = 2)
        print('[{0}] - Got average of all stack'.format(rank))
        tstop = time.time()
        print('[{0}] - done with registration in {1}.'.format(rank,(tstop-tstart)/60.))
        print('Completed in {0} files in {1} min.'.format(
            len(params['filenames']),(MPI.Wtime() - tbegin)/60.))
#        import pylab as plt
#        plt.figure()
#        plt.imshow(avgStack)
#        plt.show()
    rmtree(tmpdir)
    print('[{0}] - Removed temporary folders.'.format(rank))
    sys.exit(0)

if __name__ == '__main__':
    main()


# NEED TO HAVE THE DRIFTS
# NEED TO STORE MEAN FRAME
# NEED TO DO REGISTRATION PER 20 MIN RUN?
