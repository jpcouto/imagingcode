# Parallel registration of 2P data from scanbox files

from mpi4py import MPI
import sys
import os
from os.path import join as pjoin
from glob import glob
import numpy as np
from scipy.stats import mode
import time
from shutil import rmtree
from tifffile import imread, imsave
from tqdm import tqdm
from imaging import (strNumberSort, sbxGetShape,
                     reconstruct2PTifFile,
                     reconstruct2PStack,
                     estimate2PLineShift,
                     register2PFrames,
                     dftreg_TranslationShift,
                     shiftImage)
import cv2
cv2.setNumThreads(1)

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def _search_for_ext(exppath,extension = 'tif',
                    minFilesPerRun = 1):
    if os.path.isdir(exppath):
        tmp = strNumberSort(os.listdir(exppath))
        # List only folders with over some minFilesPerRun files
        runs = []
        for f in tmp:
            if os.path.isdir(pjoin(exppath,f)):
                if (len(glob(pjoin(exppath,f,
                                   '*.'+extension))) >= minFilesPerRun):
                    runs.append(f)
                else:
                    print('Discarding run (insuficient files):' + f)
        files = []
        for run in runs:
            files.append(np.array(strNumberSort(
                glob(pjoin(exppath,run,'*.'+extension)))))
        return runs,files
    else:
        return None,None

def _get_tiff_parameters(exppath,runs,files,selectedRuns,tempdir,nRefFiles):
    # do only channel 1 for now.    
    basenames = []
    for ii,fs in enumerate(files):
        basenames.append(np.array([os.path.basename(f)
                                   for f in fs]))
        sel = np.array([i for i, v in enumerate(
            basenames[ii]) if '_ch2_' in v])
        basenames[ii] = basenames[ii][sel]
        files[ii] = fs[sel]
                
    if not len(selectedRuns):
        selectedRuns = range(len(runs))
    runs = [runs[i] for i in selectedRuns]
    files = [files[i] for i in selectedRuns]
    basenames = [basenames[i] for i in selectedRuns]
    filerun = np.concatenate([[run]*len(fname) for run,fname in zip(runs,files)])
    files = np.concatenate(files)
    basenames = np.concatenate(basenames)
    params = dict(filepaths = files,
                  filenames = basenames,
                  runname = filerun,
                  runs = runs,
                  tempdir = tempdir,
                  destfolder = destfolder,
                  expname = os.path.split(exppath)[-1],
                  plane = np.ones_like(files,dtype = 'int'),
                  channel = np.ones_like(files,dtype = 'int')*2,
                  filesForReference = files[
                      len(files)/2-nRefFiles:
                      len(files)/2 +nRefFiles])
    return params

def _get_scanbox_parameters(exppath,runs,files,selectedRuns,tempdir,nRefFiles,nFramesPerFile = 256):
    basenames = []
    plane = []
    channel = []
    outFilenames = []
    sbxFramesMap = []
    sbxShapes = []
    sbxFiles = []
    for ii,fs in enumerate(files):
        basenames.append([])
        if not len(fs) == 1:
            raise('Wrong number of files in folder.')
        fs = fs[0]
        fileShape,nPlanes = sbxGetShape(fs)
        sbxShapes.append((fileShape,nPlanes))
        sbxFiles.append(fs)
        nFramesPerPlane = (np.int64(fileShape[3])/nPlanes)
        nChannels = fileShape[0]
        nOutFiles = np.int64(np.ceil(nFramesPerPlane/np.float(nFramesPerFile)))
        print('nOutFiles: ' + str(nOutFiles))
        outFnames = []
        run = runs[ii]
        sbxFrames = []
        planeNumber = []
        channelNumber = []
        for iPlane in range(nPlanes):
            for iFile in range(nOutFiles):
                sbxFrames.append(np.unique(np.clip(np.arange(iFile*nFramesPerFile,
                                                     (iFile+1)*nFramesPerFile)*(nPlanes) + iPlane,0,fileShape[3]-1)))
                # write the filenames for each channel
                outFnames.append([pjoin(
                    run,
                    'plane{0:03d}_ch{1:02d}'.format(iPlane+1,iChan+1),
                    '{0}_p{1:03d}_ch{2:02d}_f{3:05d}.tif'.format(run,
                                                                 iPlane+1,
                                                                 iChan+1,
                                                                 iFile)
                ) for iChan in range(nChannels)]) 
                channelNumber.append([iChan+1 for iChan in range(nChannels)])
                basenames[-1].append(fs)
                planeNumber.append(iPlane)    
        plane.append(planeNumber)
        channel.append(channelNumber)
        outFilenames.append(outFnames)
        sbxFramesMap.append(sbxFrames)
    expname = os.path.split(exppath)[-1]
    if not len(selectedRuns):
        selectedRuns = range(len(runs))
    runs = [runs[i] for i in selectedRuns]
    files = [files[i] for i in selectedRuns]
    basenames = [basenames[i] for i in selectedRuns]
    plane = [plane[i] for i in selectedRuns]
    channel = [channel[i] for i in selectedRuns]
    outFilenames = [outFilenames[i] for i in selectedRuns]
    # sbxFramesMap = [sbxFramesMap[i] for i in selectedRuns]
    # sbxShapes = [sbxShapes[i] for i in selectedRuns]
    nFiles = len(np.concatenate(basenames))
    # For each plane and run select 10 files for reference (from the middle of the run)
    tmpplane = np.concatenate(plane)
    tmprunname = np.concatenate([[run]*len(fname) for run,fname in zip(runs,outFilenames)])
    tmpfileidx = np.array(range(len(tmpplane)))
    reffileidx = []
    
    for p in np.unique(tmpplane):
        for r in np.unique(tmprunname):
            idx = np.where((tmpplane == p) & (r == tmprunname))[0]
            nFilesPlane = len(idx)
            reffileidx.append(tmpfileidx[idx[int(nFilesPlane/2-nRefFiles):int(nFilesPlane/2+nRefFiles)]])
    params = dict(filepaths = np.concatenate(basenames),
                  files = np.array([sbxFiles[i] for i in selectedRuns]),
                  sbxShapes = [sbxShapes[i] for i in selectedRuns],
                  sbxFrames = np.concatenate([sbxFramesMap[i] for i in selectedRuns]),
                  runname = np.concatenate([[run]*len(fname) for run,fname in zip(runs,outFilenames)]),
                  outpaths = np.concatenate(outFilenames),
                  runs = runs,
                  tempdir = tempdir,
                  expname = expname,
                  plane = np.concatenate(plane),
                  channel = np.concatenate(channel),
                  filesForReference = np.concatenate(reffileidx))
    return params
def _get_mpi_frame(mpifile,frame,fileShape):
    framesize = fileShape[0]*fileShape[1]*fileShape[2]*np.uint16(1).itemsize
    tmp = np.empty(framesize/np.uint16(1).itemsize, dtype=np.uint16)
    mpifile.Iread_at(framesize*frame,tmp).Wait()
    return np.transpose(tmp.reshape(fileShape[:3],order='F'),[0,2,1])

def _get_fileblock_mpi(mpifile,offset,bsize,fileShape):
    framesize = fileShape[0]*fileShape[1]*fileShape[2]*np.uint16(1).itemsize
    tmp = np.empty(framesize*bsize/np.uint16(1).itemsize, dtype=np.uint16)
    mpifile.Iread_at(framesize*offset,tmp).Wait()
    return np.ascontiguousarray(np.transpose(tmp.reshape([fileShape[0],
                                                          fileShape[1],
                                                          fileShape[2],
                                                          bsize],order='F'),[3,0,2,1]))
def main():
    desc = 'Reconstruction and 2D registration of 2P data.'

    if size < 2:
        print('''
        {0}
        Need at least 2 MPI workers...
              Run it with MPI like this:
                mpiexec -n 12 2p-sbx-reg <FOLDERNAME>
        Or use the nerf-cluster submission tools.
        '''.format(desc))
        sys.exit(1)
    params = {}

    # Get the list of available compute nodes.
    mpiNodeInfo = [rank,MPI.Get_processor_name()]
    mpiTopology = comm.gather(mpiNodeInfo,root = 0)
    ncrop = 100
    if rank == 0:
        mpiTopology = np.vstack(mpiTopology)
        print('Running with {0} workers on {1} computers.'.format(
            size,
            len(np.unique(mpiTopology[:,1]))))
        from argparse import ArgumentParser
        parser = ArgumentParser(description=desc)
        parser.add_argument('folder',metavar='foldername',
                            type=str,help='folder with raw 2P data (takes tiffs or sbx data)',
                            nargs=1)
        parser.add_argument('-o','--output-folder',metavar='destination',
                            default=None,
                            type=str,help='folder to place reg data',
                            nargs=1)
        parser.add_argument('--temp',metavar='temporary directory',
                            default=['/tmp'],
                            type=str,help='folder to place reg data',
                            nargs=1)
        parser.add_argument('--runs',metavar='run indexes',
                            action='store',
                            type=int,
                            nargs='+',
                            default=[],
                            help='runs to reconstruct/register')
        parser.add_argument('--no-reconstruction',
                            action='store_false',
                            default=True,
                            help='skips reconstruction')
        parser.add_argument('--independent-runs',
                            action='store_true',
                            default=False,
                            help='aligns each run to it-self')
        opts = parser.parse_args()
        exppath = opts.folder[0]
        if opts.output_folder is None:
            destfolder = os.path.abspath(os.path.curdir)
            print('Output not defined, using current folder: {0}'.format(destfolder))
        else:
            destfolder = opts.output_folder[0]
        selectedRuns = opts.runs
        tempdir = opts.temp[0]
        nRefFiles = 5
        runs,files = _search_for_ext(exppath,extension = 'sbx',
                                     minFilesPerRun = 1)
        if len(runs):
            recmode = 'scanbox'
            params = _get_scanbox_parameters(exppath,
                                             runs,
                                             files,
                                             selectedRuns,
                                             tempdir,
                                             nRefFiles,
                                             nFramesPerFile = 128)
        else:
            recmode = 'tif'
            runs,files = _search_for_ext(exppath,extension = 'tif',
                                         minFilesPerRun = 10)
            params = _get_tiff_parameters(exppath,
                                          runs,files,
                                          selectedRuns,
                                          tempdir,
                                          nRefFiles)
        params['destfolder'] = destfolder
        params['mode'] = recmode
        if opts.independent_runs:
            params['independentRuns'] = True
        else:
            params['independentRuns'] = False
        print('''
        Experiment: {0}
        Temp directory: {1}
        Output path: {2}
        Runs: {3}
        Files: {4}
        '''.format(params['expname'],
                   params['tempdir'],
                   params['destfolder'],
                   len(params['runs']),
                   len(params['filepaths'])))
        # If it is a sbx file need to assign the load because of file IO
        if params['mode'] == 'scanbox':
            blocks = []

            for f in np.unique(params['filepaths']):
                fidx = np.where(params['filepaths'] == f)[0]
                blocksize = len(params['sbxFrames'][fidx[0]])
                (nchans,W,H,filesize),nplanes = params['sbxShapes'][np.where(
                    params['files'] == params['filepaths'][fidx[0]])[0][0]]
                nframes = int(filesize/nplanes*nplanes)
                iblocks = np.arange(0,nframes,nplanes*blocksize)
                if iblocks[-1] < nframes:
                    iblocks = np.append(iblocks,nframes)
                framesize = nchans*W*H
                for ii,offset in enumerate(iblocks[:-1]):
                    bsize = iblocks[ii+1] - offset
                    blocks.append((params['filepaths'][fidx[0]],offset,bsize))
            #params['workBlocks'] = blocks
            params['workerBlocks'] = np.array_split(blocks,size)
        # Assign nodes to work (this is a lazy way of doing it...
        # Change it later.)
        worker = np.zeros(len(params['outpaths']),dtype=int)
        computers = np.unique(mpiTopology[:,1])
        computer = np.ones(len(params['outpaths']),dtype=int)

        nFiles = len(params['filepaths'])
        workerFiles = np.array_split(np.arange(nFiles),size)
        i = 0
        for w,f in enumerate(workerFiles):
            for j in f:
                worker[i] = w
                computer[i] = np.where(computers == mpiTopology[w,1])[0][0]
                i += 1
        tmpdirprefix = 'rec_reg_{0}'.format(int(time.time()))
        tmpdir = [pjoin(params['tempdir'],'{0}_worker{1:0d}'.format(
            tmpdirprefix,r)) for r in worker]
        params['tmpdirFiles'] = tmpdir
        params['tmpdirWorkers'] = [pjoin(
            params['tempdir'],'{0}_worker{1:0d}'.format(
                tmpdirprefix,r)) for r in range(size)]
        params['worker'] = worker
        params['workerFiles'] = workerFiles
        pworkers = []
        nPrincipalWorkers = 4
        cworkersWork = []
        # Define principal workers to deal with too many workers i/o issues
        for i,c in enumerate(computers):
            pworkers.append(np.where(mpiTopology[:,1] == c)[0][:nPrincipalWorkers])
        for i,w in enumerate(pworkers):
            cworkersWork.extend(np.array_split(
                np.where(computer == i)[0],len(w)))
        params['primaryWorkers'] = np.hstack(pworkers)
        params['primaryWorkersWork'] = cworkersWork
    params = comm.bcast(params, root=0)
    tmpdir = params['tmpdirWorkers'][rank]
    os.makedirs(tmpdir)
    comm.barrier()
    nFiles = len(params['filepaths'])
    filesPerWorker = int(np.ceil((nFiles+1)/float(size)))
    offset = filesPerWorker*rank
    nchannels = [s[0][0] for s in params['sbxShapes']]
    nplanes = [s[1] for s in params['sbxShapes']]
    if rank == 0:
        print('Starting reconstruction of {0} files.'.format(
            nFiles,filesPerWorker))
        tstart = time.time()
        tbegin = MPI.Wtime()

    if params['mode'] == 'scanbox':
        sbxfilepaths = params['files']
        # open all files with MPI (memmap is slow...)
        sbxfiles = [MPI.File.Open(comm,sbxfile,
                                  MPI.MODE_RDONLY) for sbxfile in sbxfilepaths]
        work = params['workerBlocks'][rank]
        print('[{0} - has {1} jobs]'.format(rank,len(work)))
        for ii,block in enumerate(work):
            fname,offset,bsize = block
            fdidx = np.where(
                    sbxfilepaths == fname)[0][0]
            tstart = time.time()
            sbxshape = params['sbxShapes'][fdidx][0]
            buf =  np.uint16(2**16) - _get_fileblock_mpi(sbxfiles[fdidx],
                                                         int(offset),
                                                         int(bsize),
                                                         sbxshape)
            print('[{0}] - Toke {1}, nframes = {2}, {3} of {4}'.format(rank,time.time() - tstart,
                                                                       len(buf),ii,len(work)))
        comm.barrier()
        if rank == 0:
            tstop = MPI.Wtime()
            print('[{0}] - done assigning data to nodes in {1:.1f} min. Computing reference'.format(rank,(tstop-tbegin)/60.))
            tstart = time.time()
        for s in sbxfiles:
            s.Close()
        sys.exit()
        if rank in params['primaryWorkers']:
            sbxfiles = [np.memmap(sbxfile,dtype='uint16',
                                  shape=sbxshape[0],
                                  order='F') for sbxfile,
                        sbxshape in zip(params['files'],
                                        params['sbxShapes'])]

            work = params['primaryWorkersWork'][np.where(
                params['primaryWorkers'] == rank)[0][0]]
            if rank == params['primaryWorkers'][0]:
                pbar = tqdm(total=len(work))
            for iFile in work:
                fdidx = np.where(
                    sbxfilepaths == params['filepaths'][iFile])[0][0]
                dataidx = params['sbxFrames'][iFile]
                sbxshape = params['sbxShapes'][fdidx][0]
                data = np.empty([len(dataidx),
                                 sbxshape[0],
                                 sbxshape[2],
                                 sbxshape[1]],dtype='uint16')
                #for k,ind in enumerate(dataidx):
                    #data[k,:,:,:] = np.uint16(2**16) - _get_mpi_frame(
                    #    sbxfiles[fdidx],ind,sbxshape)
                data = np.uint16(2**16) - np.array(sbxfiles[fdidx][:,:,:,dataidx])
                data = np.transpose(data,[3,0,2,1])
                for ch in range(data.shape[1]):
                    targetfile = os.path.splitext(pjoin(
                        params['tmpdirFiles'][iFile],
                        params['outpaths'][iFile][ch]))[0]+'.npy'
                    try:
                        if not os.path.isdir(os.path.dirname(targetfile)):
                            os.makedirs(os.path.dirname(targetfile))
                    except:
                        print('.')
                    np.save(targetfile,np.squeeze(data[:,ch,:,100:]))
                    N,p,w,h = data[:,:,:,100:].shape
                if rank == params['primaryWorkers'][0]:
                    pbar.update(1)
            for s in sbxfiles:
                del s
            #s.Close()
        if rank == params['primaryWorkers'][0]:
            pbar.close()
            print('Waiting for other workers...')

    else:
        # Reconstruct TIFF files!
        recdata = []
        rec_ed_files = 0
        if rank == 0:
            pbar = tqdm(total=filesPerWorker)
        for i in range(filesPerWorker):
            iFile = i+offset
            if iFile<nFiles:
                fname = params['filenames'][iFile]
                fpath = params['filepaths'][iFile]
                reconstruct2PTifFile([fpath,pjoin(tmpdir,fname)],
                                     shift=shiftEstimate[params['runs'].index(params['runname'][iFile])])
                rec_ed_files += 1
                if rank == 0:
                    pbar.update(1)
        if rank == 0:
            pbar.close()
    comm.barrier()
    if rank == 0:
        tstop = time.time()
        print('[{0}] - done assigning data to nodes in {1:.1f} min. Computing reference'.format(rank,(tstop-tstart)/60.))
        tstart = time.time()
    ########################################################################
    # Estimate average planes
    ########################################################################
    means = [None for i in range(len(params['runs']))]
    nav = [0 for i in range(len(params['runs']))]
    for iFile in params['workerFiles'][rank]:
        if iFile in params['filesForReference']:
            iRun = params['runs'].index(params['runname'][iFile])            
            for ch in range(len(params['outpaths'][iFile])):
                fpath = os.path.splitext(pjoin(tmpdir,params['outpaths'][iFile][ch]))[0]+'.npy'
                dat = np.load(fpath)
                pl = params['plane'][iFile]
                if means[iRun] is None:
                    N,w,h = dat.shape
                    means[iRun] = np.zeros((nplanes[iRun],
                                            nchannels[iRun],w,h),
                                           dtype = np.float32)
                means[iRun][pl,ch,:,:] += dat.mean(axis=0)
            nav[iRun] += 1
    for i,n in enumerate(nav):
        if not n == 0:
            means[i] /= float(n)
        elif not means[i] is None:
            means[i][:,:,:,:] = np.nan
    # Compute the reference plane
    meansReduce = comm.gather(means,root = 0)
    ref = []
    if rank == 0:
        def _reduceList(mreduce, nList = len(params['runs']),axis = 4):
            ref = [[] for i in range(nList)]
            for iRun in range(nList):
                mm = []
                for m in mreduce:
                    if not m[iRun] is None:
                        mm.append(m[iRun])
                aa = np.concatenate([np.expand_dims(
                    m,axis=4) for m in mm],
                                    axis=4)
                ref[iRun] = np.nanmean(aa,axis=4)
            return ref
        ref = _reduceList(meansReduce)
        print('[{0}] - Got average planes. Registering to average to compute references.'.format(rank))
    ref = comm.bcast(ref, root=0)

    ########################################################################
    # Register to average plane and compute new average (only on every other line)
    ########################################################################
    means = [None for i in range(len(params['runs']))]
    nav = [0 for i in range(len(params['runs']))]
    for iFile in params['workerFiles'][rank]:
        if iFile in params['filesForReference']:
            iRun = params['runs'].index(params['runname'][iFile])
            pl = params['plane'][iFile]
            for ch in range(len(params['outpaths'][iFile])):
                fpath = os.path.splitext(pjoin(tmpdir,
                                               params['outpaths'][iFile][ch]))[0]+'.npy'
                img = np.load(fpath)
                reference = ref[iRun][pl,ch,:,:]
                reged = np.zeros_like(img,dtype = np.float32)
                for iD,dim in enumerate(img):
                    shift = dftreg_TranslationShift(
                        reference[ncrop:-ncrop,ncrop:-ncrop],
                        dim[ncrop:-ncrop,ncrop:-ncrop],
                        upsample_factor=4)
                    reged[iD,:,:] = shiftImage(dim,shift)
            if means[iRun] is None:
                N,w,h = img.shape
                means[iRun] = np.zeros((nplanes[iRun],
                                        nchannels[iRun],w,h),
                                       dtype = np.float32)
            means[iRun][pl,ch,:,:] += reged.mean(axis=0)
            nav[iRun] += 1
    for i,n in enumerate(nav):
        if not n == 0:
            means[i] /= float(n)
        elif not means[i] is None:
            means[i][:,:,:,:] = np.nan
    meansReduce = comm.gather(means,root = 0)

    ref = []
    if rank == 0:
        ref = _reduceList(meansReduce)
        print('[{0}] - Computed reference planes. Going to find the shifts.'.format(rank))
    ref = comm.bcast(ref, root=0)
    ########################################################################
    # Find shifts 
    ########################################################################
    if rank == 0:
        pbar = tqdm(total=len(params['workerFiles'][rank]))
    shifts = []
    for iFile in params['workerFiles'][rank]:
        shifts.append([])
        if iFile<nFiles:
            iRun = params['runs'].index(params['runname'][iFile])
            pl = params['plane'][iFile]
            for ch in range(len(params['outpaths'][iFile])):
                shifts[-1].append([])
                fpath = os.path.splitext(pjoin(tmpdir,
                                               params['outpaths'][iFile][ch]))[0]+'.npy'
                img = np.load(fpath)
                reference = ref[iRun][pl,ch,:,:]
                reged = np.zeros_like(img,dtype = np.float32)
                for iD,dim in enumerate(img):
                    shifts[-1][-1].append(
                        dftreg_TranslationShift(reference[ncrop:-ncrop,
                                                          ncrop:-ncrop],
                                                dim[ncrop:-ncrop,
                                                    ncrop:-ncrop],
                                                upsample_factor=2))
            if rank == 0:
                pbar.update(1)
    if rank == 0:
        pbar.close()
        print('[{0}] - Computed shifts.'.format(rank))
    #######################################################################
    # Register and save
    #######################################################################
    if not params['independentRuns']:
        nshifts = comm.gather(shifts,root = 0)
        # get the shifts between reference planes
        if rank == 0:
            rshifts = []
            if iRun in range(len(params['runs'])):
                rshifts.append([])
                for pl in range(nplanes[iRun]):
                    reference = ref[0][pl,0,:,:]
                    rshifts[iRun].append(dftreg_TranslationShift(
                        reference[ncrop:-ncrop,
                                  ncrop:-ncrop],
                        ref[iRun][pl,0,ncrop:-ncrop,
                            ncrop:-ncrop],
                        upsample_factor=2))
            print(rshifts)
        comm.barrier()
        
    means = [None for ch,pl in zip(nchannels,nplanes)]
    nav = [0 for i in range(len(params['runs']))]
    if rank == 0:
        pbar = tqdm(total=len(params['workerFiles'][rank]))

    for j,iFile in enumerate(params['workerFiles'][rank]):
        iRun = params['runs'].index(params['runname'][iFile])
        pl = params['plane'][iFile]
        shift = shifts[j]
        for ch in range(len(params['outpaths'][iFile])):
            shift = shift[0] # Use the first channel to register all.
            fpath = os.path.splitext(pjoin(tmpdir,
                                           params['outpaths'][iFile][ch]))[0]+'.npy'
            img = np.load(fpath)
            reged = np.zeros_like(img,dtype = np.uint16)
            for iD,dim in enumerate(img):
                reged[iD,:,:] = shiftImage(dim,shift[iD])
            targetpath = pjoin(params['destfolder'],
                               params['expname'],
                               params['outpaths'][iFile][ch])
            if not os.path.exists(os.path.dirname(targetpath)):
                os.makedirs(os.path.dirname(targetpath))
            imsave(targetpath,reged)
            if means[iRun] is None:
                N,w,h = img.shape
                means[iRun] = np.zeros((nplanes[iRun],
                                        nchannels[iRun],w,h),
                                       dtype = np.float32)
            means[iRun][pl,ch,:,:] += reged.mean(axis=0)
            nav[iRun] += 1
        if rank == 0:
            pbar.update(1)
    for i,n in enumerate(nav):
        if not n == 0:
            means[i] /= float(n)
        elif not means[i] is None:
            means[i][:,:,:,:] = np.nan
    meansReduce = comm.gather(means,root = 0)

    if rank == 0:
        pbar.close()
        ref = _reduceList(meansReduce)
        tstop = time.time()
        print('[{0}] - done with registration in {1}.'.format(rank,(tstop-tstart)/60.))
        print('Completed in {0} files in {1} min.'.format(
            len(params['filepaths']),(MPI.Wtime() - tbegin)/60.))
    rmtree(tmpdir)
    print('[{0}] - Removed temporary folders.'.format(rank))
    comm.barrier()
    sys.exit(0)

if __name__ == '__main__':
    main()


# NEED TO HAVE THE DRIFTS
# NEED TO STORE MEAN FRAME
# NEED TO DO REGISTRATION PER 20 MIN RUN?
