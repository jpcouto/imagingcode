import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import sys
from glob import glob
import numpy as np
import os
from os.path import join as pjoin
import sys
import time

import cv2
try:
    cv2.setNumThreads(1)
except:
    pass


import caiman as cm
from caiman.motion_correction import MotionCorrect
from caiman.source_extraction.cnmf import cnmf as cnmf
from caiman.source_extraction.cnmf import params as params


def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Runs CNMF using CaImAn for a single plane.')
    parser.add_argument('folder',metavar='foldername',
                        type=str,help='single plane, single channel folder with reg data')
    
    parser.add_argument('--fr',
                        action='store',
                        default=30./4.,
                        type=float,
                        help='sampling frequency')
    
    parser.add_argument('--axons',
                        action='store_true',
                        default=False,
                        help='use initialization for axons')


    opts = parser.parse_args()


    # dataset dependent parameters
    fr = opts.fr            # imaging rate in frames per second
    decay_time = 0.7        # length of a typical transient in seconds
    
    # motion correction parameters
    strides = (48, 48)     # start a new patch for pw-rigid motion correction every x pixels
    overlaps = (24, 24)         # overlap between pathes (size of patch strides+overlaps)
    max_shifts = (6,6)          # maximum allowed rigid shifts (in pixels)
    max_deviation_rigid = 3     # maximum shifts deviation allowed for patch with respect to rigid shifts
    pw_rigid = False             # flag for performing non-rigid motion correction

    # parameters for source extraction and deconvolution
    p = 1                       # order of the autoregressive system
    gnb = 3                     # number of global background components
    merge_thr = 0.85            # merging threshold, max correlation allowed
    rf = 25                     # half-size of the patches in pixels. e.g., if rf=25, patches are 50x50
    stride_cnmf = 6             # amount of overlap between the patches in pixels
    K = 8                       # number of components per patch
    gSig = [5, 5]               # expected half size of neurons in pixels
    if not opts.axons:
        method_init = 'greedy_roi'  # initialization method (if analyzing dendritic data using 'sparse_nmf')
        is_dendrites = False        # flag for analyzing dendritic data
    else:
        method_init = 'sparse_nmf'  # initialization method (if analyzing dendritic data using 'sparse_nmf')
        is_dendrites = True         # flag for analyzing dendritic data
    ssub = 1                    # spatial subsampling during initialization
    tsub = 1                    # temporal subsampling during intialization
    
    # parameters for component evaluation
    min_SNR = 2.3               # signal to noise ratio for accepting a component
    rval_thr = 0.85              # space correlation threshold for accepting a component
    cnn_thr = 0.99              # threshold for CNN based classifier
    cnn_lowest = 0.2 # neurons with cnn probability lower than this value are rejected
    

    #%% start a cluster for parallel processing
    c, dview, n_processes = cm.cluster.setup_cluster(
        backend='local', n_processes=None, single_thread=False)
    
    tmpfolder = pjoin('/tmp',*opts.folder.split(os.path.sep)[-3:])
    tmpfolder0 = os.path.dirname(os.path.dirname(tmpfolder))
    if not os.path.isdir(tmpfolder):
        os.makedirs(tmpfolder)
    folder = opts.folder
    fnames = [f for f in np.sort(
        glob(pjoin(folder,'*.tif')))]
    if not len(fnames):
        print('Could not find data in folder: {0}'.folder)
        return
    
    opts_dict = {'fnames': fnames,
                 'fr': fr,
                 'decay_time': decay_time,
                 'strides': strides,
                 'overlaps': overlaps,
                 'max_shifts': max_shifts,
                 'max_deviation_rigid': max_deviation_rigid,
                 'pw_rigid': pw_rigid,
                 'p': 1,
                 'nb': gnb,
                 'rf': rf,
                 'K': K, 
                 'stride': stride_cnmf,
                 'method_init': method_init,
                 'rolling_sum': True,
                 'only_init': True,
                 'ssub': ssub,
                 'tsub': tsub,
                 'merge_thr': merge_thr, 
                 'min_SNR': min_SNR,
                 'rval_thr': rval_thr,
                 'use_cnn': True,
                 'min_cnn_thr': cnn_thr,
                 'cnn_lowest': cnn_lowest}

    opts = params.CNMFParams(params_dict=opts_dict)

    tmpfolder = '/tmp/caiman-tmp'
    if not os.path.isdir(tmpfolder):
        os.makedirs(tmpfolder)
    t1 = time.time()
    mmap_fnames = cm.save_memmap(fnames,
                                 base_name=pjoin(tmpfolder,'memmap_'),
                                 order = 'C', dview = dview) # exclude borders

    # now load the file
    Yr, dims, T = cm.load_memmap(mmap_fnames)    
    d1, d2 = dims
    images = np.reshape(Yr.T, [T] + list(dims), order='F')

    opts.change_params({'p': 0})
    cnm = cnmf.CNMF(n_processes, params=opts, dview=dview)
    cnm = cnm.fit(images)

    cnm.estimates.evaluate_components(images,cnm.params)

    cnm.params.change_params({'p': p})
    cnm2 = cnm.refit(images, dview=dview)

    cnm2.estimates.evaluate_components(images, cnm2.params, dview=dview)

    cnm2.estimates.detrend_df_f(quantileMin=8, frames_window=250)

    cnm2.save(pjoin(folder,'results.cnmf.hdf5'))

    cm.stop_server(dview=dview)
    print('Done in {0} min.'.format((time.time() - t1)/60.))
    log_files = glob('*_LOG_*')
    for log_file in log_files:
        os.remove(log_file)
    import shutil
    shutil.rmtree(tmpfolder0)
    print('Done cleaning up temporary files and caiman logs.')
if __name__ == '__main__':
    main()

def run_job():
    import sys
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Cluster submission script for registration script for 2P scanbox data - DASK')
    parser.add_argument('folder',metavar='foldername',
                        type=str,help='folder with reg data from one run')
    parser.add_argument('--ncpus',
                        action='store',
                        default=[40],
                        type=int,
                        nargs=1,
                        help='number of cpus')
    parser.add_argument('--fr',
                        action='store',
                        default=30.,
                        type=float,
                        help='sampling frequency')

    parser.add_argument('--axons',
                        action='store_true',
                        default=False,
                        help='use initialization for axons')
    
    opts = parser.parse_args()

    tt = list(filter(os.path.isdir,glob(pjoin(opts.folder,'*'))))
    folders = np.sort(tt)
    planes = []
    if len(folders):
        for f in folders:
            pln = glob(pjoin(f,'plane*'))
            planes.append([p for p in np.sort(list(filter(os.path.isdir,pln)))])
    else:
        raise(OSError("Check folder: {0}".format(opts.folder)))
    #planes = np.sort(glob(pjoin(opts.folder,'plane*')))
    #planes = [os.path.basename(p) for p in planes if os.path.isdir(p)]
    import subprocess as sub
    #fr = opts.fr/float(len(planes))
    nplanes = len(planes[0])
    fr = opts.fr/float(nplanes)
    for irun,planefolders in enumerate(planes):
        for iplane,planefolder in enumerate(planefolders):
            #planefolder = pjoin(opts.folder,planefolder)
            cmd = '2p-caiman-cnmf {0} --fr {1}'.format(planefolder,fr)
            if opts.axons:
                cmd += ' --axons'

            jobname = '2p-caiman{0}'.format(iplane)
            ntasks = 1
            ncpuspertask = opts.ncpus[0]
            memory = None
            walltime = None
            partition = 'short'
            conda_environment = None
            module_environment = None
            mail = None
            workdir = os.path.abspath(pjoin(os.path.expanduser('~'),'dask-temp'))
            sbatch_append = ''
            sjobfile = '''#!/bin/bash -login
#SBATCH --job-name={0}
#SBATCH --output=log_%j.out
#SBATCH --error=log_%j.err

#SBATCH --ntasks={1}
#SBATCH --cpus-per-task={2}
'''.format(jobname,ntasks,ncpuspertask)
            if not walltime is None:
                sjobfile += '#SBATCH --time={0} \n'.format(walltime)
            if not memory is None:
                sjobfile += '#SBATCH --mem={0} \n'.format(memory)
            if not partition is None:
                sjobfile += '#SBATCH --partition={0} \n'.format(partition)
            if not mail is None:
                sjobfile += '#SBATCH --mail-user={0} \n#SBATCH --mail-type=END,FAIL \n'.format(mail)
            if not module_environment is None:
                sjobfile += '\n module purge\n'
                sjobfile += '\n module load {0} \n'.format(module_environment)
            if not conda_environment is None:
                sjobfile += 'source activate {0} \n'.format(conda_environment)
            sjobfile += '''echo JOB STARTED `date`
module load conda-local
source activate 2p-py3
export MKL_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1
cd {0}
{1}
echo JOB FINISHED `date`
'''
            batchfname = pjoin(workdir,'caiman_cnmf_'+'-'.join(opts.folder.split(os.path.sep))+'{0:03d}.sh'.format(iplane))
            with open(batchfname,'w') as f:
                f.write(sjobfile.format(workdir,cmd))
            sub.call('sbatch ' + batchfname,shell=True)
