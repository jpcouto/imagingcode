# Scanbox registration using dask distributed.
# Functions and imports
import sys
import os
from os.path import join as pjoin
from glob import glob
import numpy as np
from scipy.stats import mode
import time
from shutil import rmtree
from dask import array as da
from dask import delayed
from dask.distributed import Client, progress
from cv2 import setNumThreads,warpAffine
from imaging import strNumberSort, estimate2PLineShift
from imaging.sbx import sbx_get_shape as sbxGetShape
from imaging.sbx import sbx_memmap,sbx_get_info
from scipy.ndimage.filters import correlate, convolve
import h5py as h5
from dask.array import stats as dastats
from tifffile import imsave,imread
#from skimage.feature import register_translation
from imaging import  dftreg_TranslationShift as register_translation
setNumThreads(1)

def _search_for_ext(exppath,extension = 'sbx',
                    minFilesPerRun = 1):
    if os.path.isdir(exppath):
        tmp = strNumberSort(os.listdir(exppath))
        # List only folders with over some minFilesPerRun files
        runs = []
        for f in tmp:
            if os.path.isdir(pjoin(exppath,f)):
                if (len(glob(pjoin(exppath,f,
                                   '*.'+extension))) >= minFilesPerRun):
                    runs.append(f)
                else:
                    print('Discarding run (insuficient files):' + f)
        files = []
        for run in runs:
            files.append(np.array(strNumberSort(
                glob(pjoin(exppath,run,'*.'+extension)))))
        return runs,files
    else:
        return None,None


def loadSBXblock(block,
                 shape = None,trimleft = 95,trimright=0,shiftLines = 0): 
    '''Loads a block of a scanbox file (nchannels,W,H,nplanes)
    Returning shape is a Frames x Planes x Channels X H x W array.
    Subtracts data to 2**16 and removes a chunck of artifact.'''
    fname,offset,bsize = block
    nchans,W,H,nplanes = shape
    framesize = nchans*W*H
    with open(fname,'r') as fd:
        fd.seek(offset*framesize*np.uint16().itemsize)
        buf = np.fromfile(fd,dtype = 'uint16',count=framesize*bsize)
    buf = buf.reshape((nchans,W,H,bsize),
                               order='F').transpose([3,0,2,1]).reshape(
                                   int(bsize/nplanes),
                                   nplanes,
                                   nchans,
                                   H,W)
    
    buf[:,:,:,::2,:] = np.roll(buf[:,:,:,::2,:],shiftLines,axis = -1)
    buf = buf[:,:,:,:,int(trimleft):]
    if not trimright == 0:
        buf = buf[:,:,:,:,:int(-1*trimright)]
    return 65535 - buf

def prepareSBXdaskLoad(sbxfile,blocksize = 128,trimleft = 95,trimright = 0, shiftLines = 0):
    (nchans,W,H,filesize),nplanes = sbxGetShape(sbxfile)
    # Even frames on all planes by trimming to last bit.
    nframes = int(filesize/nplanes*nplanes)
    print('\t\t Number of frames [{1}]: {0}'.format(nframes,sbxfile))
    iblocks = np.arange(0,nframes,nplanes*blocksize)
    if iblocks[-1] < nframes:
        iblocks = np.append(iblocks,nframes)
    framesize = nchans*W*H
    blocks = []
    for ii,offset in enumerate(iblocks[:-1]):
        bsize = iblocks[ii+1] - offset
        blocks.append((sbxfile,offset,bsize))
    lazyload = delayed(lambda x: loadSBXblock(x,
                                              shape=(nchans,W,H,nplanes),
                                              trimleft = trimleft,
                                              trimright = trimright,
                                              shiftLines = shiftLines), 
                       pure=True)  # Lazy version of loadSBX
    sample = loadSBXblock(blocks[0],shape=(nchans,W,H,nplanes))
    lazy_values = [lazyload(block) for block in blocks] 
    arrays = [da.from_delayed(lazy_value,           
                              dtype=sample.dtype,   
                              shape=[block[-1]/nplanes] + [s for s in sample.shape[1:]])
          for block,lazy_value in zip(blocks,lazy_values)]
    info= sbx_get_info(sbxfile)
    return (da.concatenate(arrays, axis=0), ((nchans,W,H,filesize),nplanes),info)

def regFrames(frames,ref,offset = [0,0], discardBorder = 100,iChannelReg = 0):
    nFrames,nPlanes,nChannels,height,width = frames.shape
    #shifts = np.zeros([nFrames,nPlanes,2],dtype=np.float32)
    res = np.zeros_like(frames)

    setNumThreads(1)
    for iPlane in range(nPlanes):
        reference = ref[iPlane][iChannelReg][discardBorder:-1-discardBorder,
                                             discardBorder:-1-discardBorder]
        for iFrame in range(nFrames):
            frame = frames[iFrame,iPlane,iChannelReg,
                           discardBorder:-1-discardBorder,
                           discardBorder:-1-discardBorder]
            shift,_,_ = register_translation(reference,frame,upsample_factor=6)
            #shifts[iFrame,iPlane,:] = shift[:].astype('float32')
            M = np.float32([[1,0,shift[1] + offset[1]],
                            [0,1,shift[0]+ offset[0]]])
            for iChannel in range(nChannels):
                res[iFrame,iPlane,iChannel,:,:] = warpAffine(frames[iFrame,
                                                                    iPlane,
                                                                    iChannel,
                                                                    :,:],
                                                             M,(width,height))

    return res


def computeReferenceImages(stacks,nReferenceFrames = 1200,client=None, reg = True):
    nn = int(nReferenceFrames/2) # number of frames
    delayed_ref = []
    delayed_1ref = []
    delayed_2ref = []
    for i,stack in enumerate(stacks):
        substack = stack[stack.shape[0]/2-nn:stack.shape[0]/2+nn]
        delayed_ref.append(substack.mean(axis=0))
        if reg:
            delayed_1ref.append(substack.map_blocks(regFrames,delayed_ref[-1],
                                                    dtype=np.uint16).mean(axis=0))
            delayed_2ref.append(substack.map_blocks(regFrames,delayed_1ref[-1],
                                                    dtype=np.uint16).mean(axis=0))
    if not reg:
        return client.compute(delayed_ref)
    else:
        return client.compute(delayed_2ref)

def stackToTiff(frames,filename, block_id = None, multiplane_tiffs = False):
    nFrames,nPlanes,nChannels,height,width = frames.shape
    if not multiplane_tiffs:
        for iChannel in range(nChannels):
            for iPlane in range(nPlanes):
                fname = filename.format(iPlane+1,iChannel,block_id[0])
                if not os.path.exists(os.path.dirname(fname)):
                    try:
                        os.makedirs(os.path.dirname(fname))
                    except:
                        pass
                imsave(fname,np.squeeze(frames[:,iPlane,iChannel,:,:]),
                       photometric = 'minisblack', imagej = False)
    else:
        if height > 1 and width > 1 :
            fname = filename.format(nPlanes,block_id[0])
            if not os.path.exists(os.path.dirname(fname)):
                try:
                    os.makedirs(os.path.dirname(fname))
                except:
                    pass
            imsave(fname,frames.reshape((nFrames*nPlanes*nChannels,height,width)),
                   photometric = 'minisblack', imagej = False)
    return block_id[0]*np.arange(nFrames)

def loadTiffsToDask(files):
    delayed_imread = delayed(imread, pure=True)  # Lazy version of imread
    # To load when the last file has a different dimension
    sample0 = imread(files[0])
    sample1 = imread(files[-1])

    lazy_values = []
    for f in files[:-1]:
        lazy_values.append([delayed_imread(f),sample0.shape])
    lazy_values.append([delayed_imread(files[-1]),sample1.shape])
    
    arrays = [da.from_delayed(v0,        
                              dtype=sample0.dtype, 
                              shape=v1)
              for v0,v1 in lazy_values]
    return da.concatenate(arrays, axis=0)

def local_corr(block):
    """Computes the correlation image for the input dataset Y  using a faster FFT based method
    Parameters:
    -----------
    Y:  np.ndarray (3D or 4D)
        Input movie data in 3D or 4D format
    eight_neighbours: Boolean
        Use 8 neighbors if true, and 4 if false for 3D data (default = True)
        Use 6 neighbors for 4D data, irrespectively
    swap_dim: Boolean
        True indicates that time is listed in the last axis of Y (matlab format)
        and moves it in the front
    Returns:
    --------
    Cn: d1 x d2 [x d3] matrix, cross-correlation with adjacent pixels
    
    
    This is from CaIman: https://github.com/simonsfoundation/CaImAn/blob/master/caiman/summary_images.py
    """

    Y = np.array(block).astype('float32')
    Y -= np.nanmean(Y,axis = 0)
    Y /= np.nanstd(Y,axis = 0)

    sz = np.ones((3,3),dtype='float32')
    sz[1,1] = 0

    Yconv = convolve(Y,sz[np.newaxis,:],mode='constant')
    MASK = convolve(np.ones(Y.shape[1:],dtype='float32'),sz,mode='constant')
    Cn =  (np.nanmean(Yconv*Y,axis=0)/MASK).reshape([1]+[a for a in Y.shape[1:]]).astype(np.float32)
    return Cn

def daskComputeProjections(stacks,client,chunksize = 1000,ndownsample=5, trimregds = False):
    projections = []
    downsampled = []
    
    for session in stacks:
        projections.append([])
        downsampled.append([])
        for stack in session:
            N,H,W = stack.shape
            chunks = np.arange(0,N,chunksize,dtype=int)
            if not chunks[-1] == N:
                chunks = np.append(chunks,N)
            nstack = stack.rechunk(([a for a in np.diff(chunks)], H,W))
            lcor = nstack.map_blocks(local_corr,dtype=np.float32,
                                    chunks = [1,H,W])
            if not trimregds:
                trimnframes = ((stack.shape[0]/ndownsample)*ndownsample)
            else:
                trimnframes = 1000*ndownsample
            dsstack = stack[:trimnframes,...].reshape([trimnframes/ndownsample,ndownsample] + [a for a in stack.shape[1:]])
            downsampled[-1].append(np.squeeze(dsstack.mean(axis=1).astype(np.uint16)).persist())
            dsstack = downsampled[-1][-1]
            projections[-1].append([a for a in client.persist([da.nanmean(lcor,axis=0),
                                                               dsstack.std(axis=0),
                                                               dsstack.mean(axis=0)])])
                                                               #dastats.skew(dsstack,axis=0),
                                                               #dastats.kurtosis(dsstack,axis=0)])])
    return projections,downsampled

def dsregToTiff(frames,filename, block_id = None):
    nFrames,height,width = frames.shape
    b1,b2,b3 = block_id
    fname = filename.format(b1)
    try:
        os.makedirs(os.path.dirname(fname))
    except:
        pass
    imsave(fname,np.squeeze(frames),
           photometric = 'minisblack',
           imagej = False)
    return b1*np.arange(nFrames)

def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Registration script for 2P scanbox data - DASK')
    parser.add_argument('folder',metavar='foldername',
                        type=str,help='folder with raw sbx data')
    parser.add_argument('-o','--output-folder',metavar='destination',
                        default=None,
                        type=str,help='folder to place reg data')
    parser.add_argument('-c','--client',metavar='dask client',
                        default='/home/mouselab/dask-temp/sched.json',
                        type=str,help='dask client address or file')
    parser.add_argument('--runs',metavar='run indexes ',
                        action='store',
                        type=int,
                        nargs='+',
                        default=[],
                        help='runs to register (to perform reg on - otherwise uses all) - zero based')
    parser.add_argument('--no-projections',
                        action='store_true',
                        default=False,
                        help='compute projections')
    parser.add_argument('--no-registration',
                        action='store_true',
                        default=False,
                        help='compute projections')
    parser.add_argument('--independent-runs',
                        action='store_true',
                        default=False,
                        help='use the  reference to all runs.')
    parser.add_argument('--limit-regds',
                        action='store_true',
                        default=False,
                        help='limits regds output to 1000 frames.')    
    parser.add_argument('--ndownsample',
                        action='store',
                        default=6,
                        type=int,
                        nargs=1,
                        help='number of frames to downsample')
    parser.add_argument('--blocksize',
                        action='store',
                        default=128,
                        type=int,
                        nargs=1,
                        help='number of frames to save per file')
    parser.add_argument('--alternate-multiplane',
                        action='store_true',
                        default=False,
                        help='Have multiple planes alternating (for suite2p multiplane mode for example).')
    parser.add_argument('--shift-lines',
                        action='store_true',
                        default=False,
                        help='Whether to compute and apply line shifts.')
    parser.add_argument('--no-remove-dead-cols',
                        action='store_false',
                        default=True,
                        help='Does not calculate and remove dead columns.')

    opts = parser.parse_args()
    exppath = os.path.abspath(opts.folder)
    if opts.output_folder is None:
        destfolder = os.path.abspath(os.path.curdir)
        print('Output not defined, using current folder: {0}'.format(destfolder))
    else:
        destfolder = opts.output_folder
    selectedRuns = opts.runs
    
    runs,files = _search_for_ext(exppath,extension = 'sbx',
                                 minFilesPerRun = 1)
    expname = os.path.split(exppath)[-1]
    nFramesDownsample = opts.ndownsample
    if len(selectedRuns):
        runs = [runs[i] for i in selectedRuns]
        files = [files[i] for i in selectedRuns]
    abstime = time.time()
    shiftcols = opts.no_remove_dead_cols
    rolllines = opts.shift_lines
    multiplane_tiffs = opts.alternate_multiplane
    if multiplane_tiffs:
        print('Exporting multiplane tiffs.')
    
    print('''

    Experiment: {0}
    Dask client: {1}
    Output path: {2}
    Runs: \n\t{3}


    '''.format(expname,
               opts.client,
               destfolder,
               '\n\t'.join(runs)))
    if not len(runs):
        print('Sure there are scanbox files in that folder? ' + exppath)
        sys.exit()

    print('Scanbox registration using dask.')
    print('\t Computing line shifts and columns to discard from the first run.')
    tmpsbx = sbx_memmap(files[0][0])
    colprofile = np.mean(tmpsbx[0][0][0],axis = 0)
    colidx = np.argmax(np.diff(colprofile)) + 1
    lineshift = int(-1*estimate2PLineShift(tmpsbx[:1000,0,0,100:-100,100:-100]))
    del tmpsbx
    if not rolllines:
        print('Not applying line rolls.')
        lineshift = int(0)
    if not shiftcols:
        print('Not shifting cols.')
        colidx = int(0)
    print('\t\t lineshift on the first run is {0}'.format(lineshift))
    print('\t\t column index on the first run is {0}'.format(colidx))
    print('\t Checking if files are corrupted.')
    dims = []
    for x in files:
        tmpsbx = sbx_memmap(x[0])
        dd = tmpsbx.shape
        dims.append(dd[1:])
        print('Run {0} has {1} channels, {2} planes and is {3} by {4}'.format(x[0],
                                                                              dd[1],
                                                                              dd[2],
                                                                              dd[3],
                                                                              dd[4]))
        if tmpsbx[-1].std() == 0:
            raise(ValueError('File {0} is likely corrupted.'.format(x[0])))
        del tmpsbx
    

    blocksize = opts.blocksize
    prepared = [prepareSBXdaskLoad(x[0],blocksize=blocksize,
                                   trimleft = int(colidx),
                                   trimright = int(0), 
                                   shiftLines = int(lineshift)) for x in files]
    infos = [s[2] for s in prepared]
    dims = [s[1] for s in prepared]
    stacks = [s[0] for s in prepared]
    # Connect to the dask client
    
    client = Client(scheduler_file=opts.client)        
    if not opts.no_registration:
        tstart = time.time()
        refs = [r.result() for r in computeReferenceImages(stacks,
                                                           client=client,
                                                           reg = True)]
        #imsave('/home/mouselab/test_ref.tif',refs[0][0])

        print('Refs toke: {0:.3f} s'.format(time.time() - tstart))

        # Compute shifts between runs
        errmat = np.zeros(np.array([0,1,1])*len(refs) + np.array([1,0,0])*len(refs[0]))
        shiftsmat = np.zeros(np.array([0,1,1,0])*len(refs) +
                             np.array([1,0,0,0])*len(refs[0]) + np.array([0,0,0,2]))
        for i,ref1 in enumerate(refs):
            for j,ref2 in enumerate(refs):
                for p in range(len(ref1)):
                    shiftsmat[p,i,j,:],errmat[p,i,j],_ = register_translation(np.squeeze(
                        ref1[p,0,100:-100,100:-100]),
                                                                              np.squeeze(
                                                                                  ref2[p,0,100:-100,100:-100]))

                
        # Compute the alignment subpixel correction values
        aligncorrection = []
        for i,ref in enumerate(refs):
            aligncorrection.append([])
            for r in ref:
                #shift,_,_ = register_translation(r[0,:,::2],r[0,:,1::2],upsample_factor=10)
                shift,_,_ = register_translation(r[0,:,:],r[0,:,:],upsample_factor=10)
                aligncorrection[-1].append(shift[1])
        from scipy.stats import mode
        aligncorrection = [mode(a)[0] for a in aligncorrection]
        # Plot stuff
        plots = False
        if plots:
            for ref in refs:
                fig = plt.figure()
                nplanes = ref.shape[0]
                for i in range(nplanes):
                    fig.add_subplot(np.ceil(nplanes/2.),2,i+1)
                    plt.imshow(np.squeeze(ref[i]))
            fig = plt.figure()
            for e,err in enumerate(errmat):
                nplots = len(errmat)
                ax = plt.subplot(np.ceil(nplots/2),2,e+1)
                plt.title('Plane {0}'.format(e),fontsize=10)
                im = plt.imshow(err,interpolation=None,origin='bottom',clim = [0,0.5],cmap = 'hot')
                ax.set_xticks(np.arange(len(err),dtype=int))
                ax.set_yticks(np.arange(len(err),dtype=int))
                if e < nplots-2:
                    ax.set_xticks([])
                else:
                    plt.xlabel('# run')
                if e == nplots-1:
                    cb = plt.colorbar(im,shrink = 0.5, pad = 0.1)
                plt.suptitle('Error between sessions',fontsize=14)

        if not opts.independent_runs:
            selectedRun = int(len(runs)/2)
            print('Registering to run {0}'.format(selectedRun))
            refs = [refs[selectedRun] for ref in refs]
    
        # Register all sessions
        regstacks = []
        savestacks = []
        for i,(ref,stack) in enumerate(zip(refs,stacks)):
            regstacks.append(stack.map_blocks(regFrames,ref,
                                              dtype=np.uint16))
            fname = os.path.basename(os.path.splitext(files[i][0])[0])
            if multiplane_tiffs:
                fname = os.path.abspath(pjoin(destfolder,
                                              expname,
                                              runs[i],
                                              fname+'_volumeplanes_{0:03d}_{1:04d}.tif'))
            else:
                fname = os.path.abspath(pjoin(destfolder,
                                              expname,
                                              runs[i],
                                              'plane{0:03d}_ch{1:02d}',
                                              fname+'_{2:04d}.tif'))
                
                # recfname = os.path.abspath(pjoin(destfolder.replace('/reg','/rec'),expname,runs[i],'plane{0:03d}_ch{1:02d}',fname+'_{2:04d}.tif'))
                # savestacks.append(stack.map_blocks(stackToTiff,recfname,drop_axis=[1,2,3,4]))
            savestacks.append(regstacks[-1].map_blocks(stackToTiff,
                                                       fname,
                                                       multiplane_tiffs = multiplane_tiffs,
                                                       drop_axis=[1,2,3,4]))

        tstart = time.time()
        res = [r.result() for r in client.compute(savestacks)]
        print('Reg frames toke: {0:.3f} min'.format((time.time() - tstart)/60.))

        print('Completed registration in: {0:.3f} min'.format((time.time() - abstime)/60.))

    if multiplane_tiffs:
        print('Skipping projections (alternate multiplane mode).')
        return
    
    if opts.no_projections:
        return
    
    print('Computing projections from reg data.')
    client.restart()
    regfolder  = pjoin(destfolder,expname)
    sessionfolders = np.sort(os.listdir(regfolder))
    runs = []
    files = []
    for r in sessionfolders:
        if os.path.isdir(pjoin(regfolder,r)):
            planefolders = np.sort(os.listdir(pjoin(regfolder,r)))
            for p in planefolders:
                if os.path.isdir(pjoin(regfolder,r,p)):
                    f = np.sort(glob(pjoin(regfolder,r,p,'*.tif')))
                    if not r in runs:# and not 'stack' in r:
                        runs.append(r)
                        files.append([])
                        print('\t' + r)
                    print('\t\t' + p)
                    files[-1].append(f)
    stacks = []
    for ses in files:
        stacks.append([])
        for planes in ses:
            stacks[-1].append(loadTiffsToDask(planes))
    trimMaxFrames = False
    # This does not work for some reason... test in notebook.
    if trimMaxFrames:
        for iSes,s in enumerate(stacks):
            for iPlane,p in enumerate(s):
                stacks[iSes][iPlane] = p[np.clip(int((2**16)/float(len(s))),0,len(p)-1)]
        print('Trimming max frames to 2**16')
            
    tstart = time.time()
    projections,downsampled = daskComputeProjections(
        stacks,
        client,
        ndownsample = nFramesDownsample,
        trimregds = opts.limit_regds)
    # Wait for it..
    projections = [[np.dstack([t.compute() for t in p]) 
                    for p in pp] for pp in projections]
    print('Downsampling on cluster and computing projections toke: {0} min'.format((time.time() - tstart)/60))
    # Save projections to reg_ds
    dsdestfolder = os.path.abspath(pjoin(os.path.split(os.path.abspath(destfolder))[0],'reg_ds'))
    projfiles = []
    for filesrun,projection in zip(files,projections):
        projfiles.append([])
        for plane,proj in zip(filesrun,projection):
            #dirname = os.path.dirname(plane[0])
            dirname = os.path.abspath(os.path.dirname(
                plane[0].replace(os.path.abspath(destfolder),dsdestfolder)))
            fname = dirname+'.tifproj'
            projfiles[-1].append(fname)
            try:
                os.makedirs(os.path.dirname(fname))
            except:
                pass
            imsave(fname,proj.astype(np.float32).transpose([2,0,1]),
                   photometric = 'minisblack', imagej = False)

    # Save downsampled stacks...
    dsfiles = []
    tasks = []
    for filesrun,dsstacks in zip(files,downsampled):
        dsfiles.append([])
        for plane,dsstack in zip(filesrun,dsstacks):
            dirname = os.path.abspath(os.path.dirname(plane[0].replace(os.path.abspath(destfolder),dsdestfolder)))
            fname = os.path.basename(dirname)
            tmp = fname.split('_')
            tmp = '_'.join(tmp[:-1]+['{0:04d}.tif'])
            fname = pjoin(dirname,tmp)
            dsfiles[-1].append(fname)
            dsstack_rechunked = dsstack.rechunk([400,dsstack.shape[1],dsstack.shape[2]])
            tasks.append(dsstack_rechunked.map_blocks(dsregToTiff,dsfiles[-1][-1],drop_axis=[1,2]))
    
    [t.result() for t in client.compute(tasks)]
    print('Completed registration and projections in: {0:.3f} min'.format((time.time() - abstime)/60.))


if __name__ == '__main__':
    main()

def run_job():
    import sys
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Cluster submission script for registration script for 2P scanbox data - DASK')
    parser.add_argument('folder',metavar='foldername',
                        type=str,help='folder with raw sbx data')
    parser.add_argument('-o','--output-folder',metavar='destination',
                        default=None,
                        type=str,help='folder to place reg data')
    parser.add_argument('-c','--client',metavar='dask client',
                        default='/home/mouselab/dask-temp/sched.json',
                        type=str,help='dask client address or file')
    parser.add_argument('--runs',metavar='run indexes ',
                        action='store',
                        type=int,
                        nargs='+',
                        default=[],
                        help='runs to register (to perform reg on - otherwise uses all) - zero based')
    parser.add_argument('--no-projections',
                        action='store_true',
                        default=False,
                        help='compute projections')
    parser.add_argument('--no-registration',
                        action='store_true',
                        default=False,
                        help='compute projections')
    parser.add_argument('--independent-runs',
                        action='store_true',
                        default=False,
                        help='use the  reference to all runs.')
    parser.add_argument('--limit-regds',
                        action='store_true',
                        default=False,
                        help='limit regds to 1000 frames.')
    parser.add_argument('--ndownsample',
                        action='store',
                        default=6,
                        type=int,
                        nargs=1,
                        help='number of frames to downsample')
    parser.add_argument('--blocksize',
                        action='store',
                        default=128,
                        type=int,
                        nargs=1,
                        help='number of frames to save per file')
    parser.add_argument('--alternate-multiplane',
                        action='store_true',
                        default=False,
                        help='Have multiple planes alternating (for suite2p multiplane mode for example).')
    parser.add_argument('--shift-lines',
                        action='store_true',
                        default=False,
                        help='Whether to compute and apply line shifts.')
    parser.add_argument('--no-remove-dead-cols',
                        action='store_false',
                        default=True,
                        help='Does not calculate and remove dead columns.')
    parser.add_argument('--ncpus',
                        action='store',
                        default=[80],
                        type=int,
                        nargs=1,
                        help='number of cpus')

    opts = parser.parse_args()

    if not opts.folder == 'notebook':
        cmd = '2p-sbx-reg {0} -o {1} '.format(opts.folder,opts.output_folder)
        if len(opts.runs) > 0:
            cmd += '--runs {0} '.format(' '.join([str(r) for r in opts.runs]))
        if opts.no_projections:
            cmd += '--no-projections '
        if opts.no_registration:
            cmd += '--no-registration '
        if opts.independent_runs:
            cmd+= '--independent-runs '
        if opts.limit_regds:
            cmd+= '--limit-regds '
        if opts.alternate_multiplane:
            cmd+= '--alternate-multiplane '
        if opts.shift_lines:
            cmd += '--shift-lines '
        if not opts.no_remove_dead_cols:
            cmd+= '--no-remove-dead-cols '
    else:
        cmd = 'jupyter notebook --no-browser --ip="*"'
    import subprocess as sub
    jobname = '2p-sbx-reg'
    ntasks = opts.ncpus[0]
    ncpuspertask = 1
    memory = None
    walltime = None
    partition = 'medium'
    conda_environment = None
    module_environment = None
    mail = None
    workdir = os.path.abspath(pjoin(os.path.expanduser('~'),'dask-temp'))
    sbatch_append = ''
    sjobfile = '''#!/bin/bash -login
#SBATCH --job-name={0}
#SBATCH --output=log_%j.out
#SBATCH --error=log_%j.err

#SBATCH --ntasks={1}
#SBATCH --cpus-per-task={2}
'''.format(jobname,ntasks,ncpuspertask)
    if not walltime is None:
        sjobfile += '#SBATCH --time={0} \n'.format(walltime)
    if not memory is None:
        sjobfile += '#SBATCH --mem={0} \n'.format(memory)
    if not partition is None:
        sjobfile += '#SBATCH --partition={0} \n'.format(partition)
    if not mail is None:
        sjobfile += '#SBATCH --mail-user={0} \n#SBATCH --mail-type=END,FAIL \n'.format(mail)
    if not module_environment is None:
        sjobfile += '\n module purge\n'
        sjobfile += '\n module load {0} \n'.format(module_environment)
    if not conda_environment is None:
        sjobfile += 'source activate {0} \n'.format(conda_environment)
    sjobfile += '''echo JOB STARTED `date`
export XDG_RUNTIME_DIR=""
export MKL_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1
module load purge
module load conda-local
source activate 2p
cd {0}
# Run the scheduler
dask-scheduler --scheduler-file {0}/sched.json --local-directory /tmp/dask-local  --bokeh-whitelist "*" --host $SLURMD_NODENAME &
srun  -n {2} /opt/anaconda2/envs/2p/bin/dask-worker --scheduler-file {0}/sched.json --nthreads 1 --nprocs 1 &
sleep 2

{1}
killall srun
killall dask-scheduler
rm -r worker-*
echo JOB FINISHED `date`
'''
    with open(pjoin('dasktask.sh'),'w') as f:
        f.write(sjobfile.format(workdir,cmd,ntasks))
    sub.call('sbatch dasktask.sh',shell=True)
    
