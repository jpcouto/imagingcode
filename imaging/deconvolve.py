try:
    from oasis_deconvolve import deconvolve as oasisdeconv
except:
    pass
from tqdm import tqdm
import numpy as np

def deconvolve(ratio):
    denoised = []
    background = []
    deconvolved_rate = []
    for i in tqdm(range(ratio.shape[0]),desc='Deconvolving dF/F'):
        c, s, b, g, lam = oasisdeconv(ratio[i,:].astype('double'),
                                      g=(None,None),
                                      penalty=1,
                                      optimize_g=5)
        denoised.append(c)
        background.append(b)
        deconvolved_rate.append(s)
    deconvolved_rate = np.vstack(deconvolved_rate)
    denoised = np.vstack(denoised)
    background = np.vstack(background)
    return deconvolved_rate, denoised, background
