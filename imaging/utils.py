from glob import glob
import os
import sys
import json
from os.path import join as pjoin
from subprocess import call

preferencepath = pjoin(os.environ['HOME'],'.imagingCode')

defaultPreferences = {'paths':{'tmp':'/quadraraid/temporary',
                               'remotedata':['/mnt/nerffs01/mouselab/data',
                                             '/mnt/bkrunch/h/data'],
                               'localdata':'/quadraraid/data',},
                      'user':'JC'}


def getUserPreferences():
    ''' Reads the user parameters from the home directory.

    pref = read_user_preferences(expname)

    User parameters like folder location, file preferences, paths...
    Joao Couto - May 2017
    '''
    if not os.path.isdir(preferencepath):
        os.makedirs(preferencepath)
        print('Creating .preference folder ['+preferencepath+']')

    preffile = pjoin(preferencepath,'preferences.json')
    if not os.path.isfile(preffile):
        with open(preffile, 'w') as outfile:
            json.dump(defaultPreferences, outfile, sort_keys = True, indent = 4)
            print('Saving default preferences to: ' + preffile)
    with open(preffile, 'r') as infile:
        pref = json.load(infile)
    return pref

def getExperimentFolders(expname,runSelection = None):
    prefs = getUserPreferences()
    # List is the possible places to look for data in [[place1],[place2]]
    datafolders = {'2pRaw':[['2photon','raw']],
                   '2pRec':[['2photon','rec']],
                   '2pReg':[['2photon','reg']],
                   'eyecam':[['eyecam']],
                   'facecam':[['facecam']],
                   '1pRaw':[['1photon','raw']],
                   'stimlog':[['presentation']]}
    # runs in the experiment

    #

def openTiffSequenceOnImageJ(fname):
    macrofname = pjoin(preferencepath,'tmpmacro.ijm')
    
    with open(macrofname,'w') as f:
        f.write('run("Image Sequence...", "open={0} sort");'.format(fname))
    call('imagej --allow-multiple -macro {0} &'.format(macrofname),shell=True)
    


def setColorbarNTicks(cb,nticks=3):
    from matplotlib import ticker
    tick_locator = ticker.MaxNLocator(nbins=nticks)
    cb.locator = tick_locator
    cb.update_ticks()

def playStackNB(data,cmap = 'gray'):
    import pylab as plt
    from ipywidgets import widgets
    from IPython.display import display
    im = plt.imshow(data[0],cmap = cmap )
    slider = widgets.IntSlider(0,min = 0,
                               max = len(data)-1,
                               step = 1,
                               description='Frame')
    display(slider)

    def updateImage(change):
        im.set_data(data[change['new']])
    slider.observe(updateImage, names='value')
