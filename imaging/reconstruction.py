import numpy as np
try:
    from tifffile import imread,imsave,TiffWriter
except:
    pass
from .registration.registration import *
from os.path import join as pjoin
from tempfile import mkdtemp
import os
from multiprocessing import Pool
from functools import partial
import time
from glob import glob


def estimate2PLineShift(data):
    idxOdd = np.arange(0,data.shape[1],2)
    (ys,xs),_,_ = dftreg_TranslationShift(data[:,idxOdd,:].mean(axis=0),
                                                data[:,idxOdd+1,:].mean(axis=0))
    return xs

def reconstruct2PStack(data,nFlybackLines = 31,shift = 0,lineWidth = 720):
    nFrames,nCycles,nSamples = data.shape[:3]
    period = nSamples
    delta = 2*np.pi/period
    phi = np.arange(0,np.round(period/2)-1)*delta # scanner phase
    binningTable = np.round((1-np.cos(phi))*(lineWidth-1)/2.).astype('uint16')
    recdata = np.zeros((nFrames,nCycles*2,lineWidth),dtype='uint16')
    idxOdd = np.arange(0,nCycles*2,2)
    for i in xrange(lineWidth):
        idx = np.where(binningTable==i)[0]
        recdata[:,idxOdd,i] = np.squeeze(np.mean(np.take(data,idx,axis=2),axis = 2).astype('uint16'))
        idx=np.unique(np.clip(nSamples/2+np.where(np.flipud(binningTable==i))[0]+shift,0,nSamples-1))
        recdata[:,idxOdd + 1,i] = np.squeeze(np.mean(np.take(data,idx,axis=2),axis = 2).astype('uint16'))
    return recdata

def reconstruct2PTifFile(fnames,shift = 0):
    ''' fnames is the filename,destination '''
    filename,destination = fnames

    if os.path.isfile(destination):
        raise
    data = imread(filename)
    recdata = reconstruct2PStack(data,shift=shift)
    
    with TiffWriter(destination) as tf:
        for dd in range(recdata.shape[0]):
            tf.save(recdata[dd,:,:].astype('uint16'))


def reconstruct2PTifFiles(files, destinationPath,nworkers = 16):
    data = []
    for f in files[:3]:
        dat = imread(f)
        data.append(ic.reconstruct2PStack(dat))
    data = np.concatenate(data,axis=0)

    shift = ic.estimate2PLineShift(data)

    print('Line shift estimate: {0} pixels'.format(shift))
    recfilenames = [pjoin(destinationPath,f.split('/raw/')[-1]) for f in files]
    tstart = time.time()
    
    func = partial(reconstruct2PTifFile, shift = shift)
    if not os.path.isdir(os.path.dirname(recfilenames[0])):
        os.makedirs(os.path.dirname(recfilenames[0]))
        print('Created '+os.path.dirname(recfilenames[0]))
        
    print('Starting reconstruction with {0} workers [{1} files].'.format(nWorkers,len(files)))
    pool = Pool(nworkers)
    res = pool.map_async(func,zip(files,recfilenames))
    pool.close()
    pool.join()
    print('Done in {0} min'.format((time.time()-tstart)/60.))
