from scipy.interpolate import interp1d
import numpy as np

def interpolateToTime(x,y,nx):
    f = interp1d(x,y,bounds_error=False,fill_value=np.nan)
    return f(nx)

