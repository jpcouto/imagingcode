# Read files from the old mouselab processing stream

from os.path import join as pjoin

def loadBkrunchSegmentationResults(expname,runname,analysisPath = '/mnt/bkrunch/g/mousebox/analysis/'):
    fname = pjoin(analysisPath,expname,runname,'masks_neurons.mat')
    fname2 = pjoin(analysisPath,expname,runname,'timecourses.mat')
    
    try:
        from scipy.io import loadmat
        mask = loadmat(fname)['maskNeurons']
        try:
            ratio = loadmat(fname2)['ratio']
        except KeyError:
            ratio = loadmat(fname2)['tcs']
            ratio = ratio['ratio'][0,0]
    except NotImplementedError:
        from h5py import File as h5file
        mask = h5file(fname,'r')['maskNeurons'][:]
        ratio = h5file(fname2,'r')['ratio'][:].T
    return mask,ratio
