# Tools to access and process presentation log files

import pandas as pd
try:
    from cStringIO import StringIO
except:
    try:
        from StringIO import StringIO
    except ImportError:
        from io import StringIO

import sys
import numpy as np
import os
from glob import glob

def parsePresentationLog(logfile,beltlength = 150):
    '''
    USAGE:
    log = parse_presentation_log(logfile)

    Joao Couto - May 2017
    '''
    if not os.path.exists(logfile):
        raise(ValueError('File '+ logfile +' does not exist.'))
    generallog = ''
    picturelog = ''
    # Parse logfile
    with open(logfile,'r') as f:
        text = f.readline()
        scenario = text.split('-')[1].strip()
        text = f.readline()
        text = f.readline()
        header = f.readline()
        # Handle some messy stuff in the log
        generallog +=  header.replace(' ','').replace('(num)','')
        while True:
            text = f.readline()
            splittext = text.split('\t')
            if not 'Picture' in splittext[0]:
                generallog += text
            else:
                picturelog += text
            if not text:
                break
    # Process the data
    df = pd.read_table(StringIO(generallog))
    portinputs = {}
    pulseinputs = {}
    cnt = 0
    for event in df.EventType.unique():
        tmpdf = df[df.EventType == event]
        if len(tmpdf):
            if 'Port Input' in event:
                # Get the timestamps and convert times...
                portinputs['ch'+str(cnt)] = np.unique(tmpdf.Time.astype(float).values * 1e-4)
                cnt += 1
            elif 'Pulse' in event:
                pulseinputs = np.unique(np.unique(tmpdf.Time.astype(float).values * 1e-4))
            elif 'Picture' in event:
                stimdf = tmpdf.copy()
                stimdf.Time = stimdf.Time.astype(float) * 1e-4
            elif 'Manual' in event:
                annot = np.array(tmpdf.Code)
                nannot = {}
                for a in annot:
                    a = [b.strip() for b in a.split('=')]
                    try:
                        nannot[a[0]] = np.int(a[1])
                    except ValueError:
                        try:
                            nannot[a[0]] = np.float(a[1])
                        except ValueError:
                            nannot[a[0]] = a[1]
    stimdf.Time = stimdf.Time.astype(float)
    stimdf.iTrial = stimdf.iTrial.astype(int)
    stimdf.iFrame = stimdf.iFrame.astype(int)
    stimdf.iStim = stimdf.iStim.astype(int)
    stimdf.fBlank = stimdf.fBlank.astype(int)
    # Get stims start and stop
    uniqueTrials = stimdf.iTrial.unique()
    uniqueStims = stimdf.iStim.unique()
    vstim = []
    for iStim in uniqueStims:
        vstim.append([])
        for iTrial in uniqueTrials:
            idx = np.where((stimdf.iTrial == iTrial) & 
                           (stimdf.iStim == iStim) & 
                           (stimdf.fBlank == 0))[0]
            if not len(idx):
                print('There was a condition {0}/{1} that had no frames.'.format(iTrial,iStim))
                continue
            # Take care of repetitions
            frameTimes = np.array(stimdf.Time.iloc[idx])
            tmp = np.diff(np.hstack([idx[0] - 2,idx,idx[-1]+2]),2)
            starts = frameTimes[np.where(tmp<0)]
            ends = frameTimes[np.where(tmp>0)]
            for s,e in zip(starts,ends):
                vstim[-1].append([s,e])
        vstim[-1] = np.array(vstim[-1])
    
    t,d,v,p = behaviorFromEncoderTicksAndPhotosensor(portinputs['ch0'],portinputs['ch1'],
                                                     portinputs['ch2'],beltlength = beltlength)
    behav = dict(lap = portinputs['ch2'],
                time = t,
                distance = d,
                position=p,
                velocity = v)
    log = dict(stim_folder = scenario,
               vstim = vstim,
               log = df,
               stimlog = stimdf,
               annotations = nannot,
               ports = portinputs,
               pulses = pulseinputs,
               behav = behav)
    
    return log

from scipy.interpolate import interp1d
import scipy.signal as signal

def behaviorFromEncoderTicksAndPhotosensor(enc1,enc2,lap,beltlength = 150,
                                          sigma = 3):
    ''' Does a lot of magic..
    '''
    distance = np.zeros(enc1.shape)
    for i in range(len(enc1)-1):
        if np.mod(len(enc2[(enc2 > enc1[i]) & (enc2 < enc1[i+1])]),2):
            distance[i+1] = +1;
        else:
            distance[i+1] = -1;
    if (len(distance)<2):
        print('Not enough pulses on the encoders to compute velocity.')
        return None
    position = np.cumsum(distance)
    # Normalize position to laps        
    vsrate = 1./np.min(np.diff(enc1))    
    vtime = np.arange(min(enc1),max(enc1),1./vsrate)
    fpos = interp1d(enc1,position)
    pos = fpos(vtime)
    def normpdf(x, mu, sigma):
        return 1/(sigma*np.sqrt(2*np.pi))*np.exp(-1*(x-mu)**2/2*sigma**2)

    npos = pos.copy()
    time = vtime.copy()
    mlapticks = []
    if len(np.where(np.diff(lap) < 1)[0]):
        print('There are laps shorter than 1 second.. removing them.')
        lap = np.delete(lap,np.where(np.diff(lap) < 1)[0])
    for i,(s,e) in enumerate(zip(lap[:-1],lap[1:])):
        tmpidx = np.where((time>=s) & (time<e))[0]
        tmp = npos[tmpidx]
        npos[tmpidx] = (i+1.)+(tmp - tmp[0])/(tmp[-1] - tmp[0])
        mlapticks.append(tmp[-1] - tmp[0])
    print('''    Median lap ticks: {0}
    Mean lap ticks: {1}
    Std lap ticks: {2}
    Min Max: {3},{4}
    '''.format(int(np.median(mlapticks)),
               int(np.nanmean(mlapticks)),
               int(np.std(mlapticks)),
               int(np.nanmin(mlapticks)),
               int(np.nanmax(mlapticks))))
    if np.nanmin(mlapticks>250):
        print('\n'.join([str(int(l)) for l in mlapticks]))
    # the first lap (Assuming trigger on lap)
    tmpidx = np.where((time<lap[0]))[0]
    tmp = npos[tmpidx]
    npos[tmpidx] = tmp/tmp[-1]
    # the last lap (Assuming trigger on lap)
    tmpidx = np.where((time>lap[-1]))[0]
    tmp = npos[tmpidx]
    npos[tmpidx] = len(lap) + (tmp - tmp[0])/np.mean(mlapticks)
    distance = npos*beltlength
    position = np.mod(distance,beltlength)
    x = np.arange(-3*sigma*1e-3,-3*sigma*1e-3,1/vsrate)
    kern = normpdf(np.arange(-5*sigma*1.0e-3,5*sigma*1.0e-3,1/vsrate),0,sigma*1.0e-3)

    velocity = signal.convolve(np.diff(distance)/(vtime[1]-vtime[0]),
                               kern/sum(kern),'same')
    return time,distance,velocity,position

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()
    '''stim_folder
    vstim
    log
    stimlog
    annotations
    ports
    pulses
    behav
    '''
    import os
    import pylab as plt
    import numpy as np
    import imaging as ic
    if not os.path.isfile(args.filename):
        print(args['filename'] + ' not found.')
        return
    log = parsePresentationLog(args.filename)
    print('Recorded {0} laps.'.format(len(log['behav']['lap'])))
    plt.matplotlib.style.use('ggplot')
    fig = plt.figure()
    ax = fig.add_axes([0.1,0.1,0.8,0.1])

    time = log['behav']['time']
    vel = log['behav']['velocity']
    pos = log['behav']['position']
    laptimes = log['behav']['lap']
    del log
    plt.plot(time[:-1],vel)
    plt.xlabel('Time [s]')
    plt.xlabel('Velocity [cm/s]')
    fig.add_axes([0.1,0.3,0.8,0.1],sharex=ax)
    plt.plot(time,pos)
    plt.ylabel('Lap position [cm]')
    fig.add_axes([0.1,0.5,0.3,0.4])
    laps = np.vstack([laptimes[:-1],laptimes[1:]]).T
    lapspace = np.arange(0,150,1)
    lapX = ic.analysis.binToLaps(laps,time[:-1],
                                 pos[:-1],
                                 np.expand_dims(vel,1).T,
                                 lapspace=lapspace)
    for lap in np.squeeze(lapX).T:
        plt.plot(lapspace[:-1],lap,color='black',alpha=0.5,lw=0.6)
    plt.plot(lapspace[:-1],np.nanmean(np.squeeze(lapX),axis=1),color='red')
    plt.xlabel('Lap space [cm]')
    plt.ylabel('Average velocity [cm/s]')
    fig.add_axes([0.5,0.5,0.4,0.4])
    im = plt.imshow(np.squeeze(lapX).T,clim=[0,25],aspect='auto',
                    origin='bottom',cmap='hot',extent=[0,lapspace[-1],
                                                       0,lapX.shape[2]],
                    interpolation='none')
    plt.xlabel('Lap space [cm]')
    plt.ylabel('Lap #')
    plt.colorbar(im,shrink=0.5,label='[cm/s]')
    plt.show()
if __name__ == '__main__':
    main()
